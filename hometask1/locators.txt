﻿Google search page  :
requirements

1.query input   
2.'search in Google' button 
3.'I am lucky!' button
4.Search for 'nyan cat' and create a locator that returns ALL 10 result links
5.n-th 'o' letter in Goooooooooogle
----------------------
locators
1.$x (".//input[@id='lst-ib']")
$$ ("input#lst-ib")
2.$x (".//input[@name='btnK']") 
$$ ("input[name='btnK']")
$x (".//button[@name='btnG']")
3.$x (".//input[@name='btnI']")
$$ ("input[name='btnI']")
4.$x (".//h3[@class]/a | .//div[@class='_Icb _kk _wI']/a ")
$$ ("h3>a,div[class='_Icb _kk _wI']>a")
5.$x (".//div[@id='navcnt']//*[text()='n']")
//наверное плохо по тексту ,но там динамически меняются блоки смотря какая выбрана страница
//n - [1...10]
/css selector не смог связать ,по тектсу нету там ,а из-за того ,что страницы в зависимости какая выбрана
менятся и содержимое


---------------------
Mail.ru login page:
requirements

1.login input
2.password input
3.‘Enter’ button
---------------------
1.$x (".//input[@id='mailbox__login']")
$$ ("input#mailbox__login")
2.$x (".//input[@id='mailbox__password']")
$$ ("input#mailbox__password")
3.$x (".//input[@id='mailbox__auth__button']")
$$ ("input#mailbox__auth__button")
---------------------

Mail.ru main page (logged in):
requirements

1.links to folders (incoming, outcoming, spam, deleted, drafts)
2.action buttons (write new letter, delete, mark as spam, mark as not spam, mark as read, move to another folder)
3.Checkbox for one exact letter
4.Opening link for one exact letter
5.New letter page: inputs for address, topic, text, file attach
---------------------------
1.$x (".//a[@class='b-nav__link js-shortcut'] | .//a[@class='b-nav__link']")
$$ ("a[class='b-nav__link js-shortcut'],a[class='b-nav__link']")
2.$x (".//a[@data-name='newtab']")
$$ ("a[data-name='newtab']")

$x (".//a[@data-name='remove']")
$$ ("a[data-name='remove']")

$x (".//a[@data-name='archive']")
$$ ("a[data-name='archive']")

$x (".//a[@data-name='spam']")
$$ ("a[data-name='spam']")

$x (".//a[@data-name='moveTo']")
$$ ("a[data-name='moveTo']")

$x (".//a[@data-name='create_filter' and @class='b-dropdown__list__item']")
$$ ("a[data-name='create_filter'][class='b-dropdown__list__item']")

$x (".//a[@data-name='unread' and @class='b-dropdown__list__item']")
$$ ("a[data-name='unread'][class='b-dropdown__list__item']")

$x (".//a[@data-name='search' and @class='b-dropdown__list__item']")
$$ ("a[data-name='search'][class='b-dropdown__list__item']")

3.$x (".//div[@data-id='13385281990000000013']//div[@class='b-checkbox__box']")
$x (".//div[@data-id='13385281990000000013']//div[@class='b-checkbox__box']")
$$ ("div[data-id='13385281990000000013'] div[class='b-checkbox__box']")
// data-id  - уникальный id для определенного письма
4.$x (".//div[@class='b-datalist__item__info']/div[text()='name']")
//name - имя от кого пришло
//css снова не нашел к чему привязать(
5.
-inputs for address
$x (".//textarea[@class='js-input compose__labels__input' and @data-original-name='To']")
$$ ("textarea[class='js-input compose__labels__input'][data-original-name='To']")

-topic
$x (".//input[@class='compose__header__field']")
$$ ("input[class='compose__header__field']")

-text
$x (".//body[@id='tinymce']")
$$ ("body[id='tinymce']")

-file attach
$x (".//input[@class='js-shortcut compose__uploader__input']")
$$ ("input[class='js-shortcut compose__uploader__input']")






  
