package com.epam.gomel.homework.product.mailru.account.bo;

import lombok.Getter;
import lombok.Setter;

/**
 * Created by Uladzimir Shabetnik on 08.10.2016.
 */
public class MailRuAccount {
    @Getter @Setter
    private String username;
    @Getter @Setter
    private String password;

}
