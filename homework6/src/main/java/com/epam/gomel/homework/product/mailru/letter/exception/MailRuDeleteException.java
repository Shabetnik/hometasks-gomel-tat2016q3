package com.epam.gomel.homework.product.mailru.letter.exception;

import com.epam.gomel.homework.framework.exception.CommonTestRuntimeException;

/**
 * Created by Uladzimir Shabetnik on 09.10.2016.
 */
public class MailRuDeleteException extends CommonTestRuntimeException {

    public MailRuDeleteException(String message) {
        super(message);
    }
}
