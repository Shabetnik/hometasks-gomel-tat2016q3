package com.epam.gomel.homework.framework.ui.element;

import com.epam.gomel.homework.framework.logging.Log;
import com.epam.gomel.homework.framework.ui.browser.Browser;
import lombok.Getter;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

import java.io.File;

/**
 * Created by Uladzimir Shabetnik on 08.10.2016.
 */
public class Element {
    @Getter
    private By by;

    public Element(By by) {
        this.by = by;
    }

    public WebElement getWrappedWebElement() {
        return Browser.getBrowser().findElement(by);
    }

    public void typeValue(String value) {
        Log.info("Type value for " + by);
        WebElement webElement = getWrappedWebElement();
        webElement.clear();
        webElement.sendKeys(value);
    }

    public void click() {
        Log.info("Click on " + by);
        getWrappedWebElement().click();
    }

    public boolean isVisible() {
        return Browser.getBrowser().isElementVisible(by);
    }

    public String getText() {
        Log.info("Get text from " + by);
        return getWrappedWebElement().getText();
    }

    public void waitForVisible() {
        Browser.getBrowser().waitForElementIsVisible(by);
    }

    public void waitForAppear() {
        Browser.getBrowser().waitForElementIsAppear(by);
    }

    public void waitForDisappear() {
        Browser.getBrowser().waitForElementIsInvisible(by);
    }

    public void switchToFrame() {
        Log.info("Switch to frame " + by);
        Browser.getBrowser().switchToFrameElement(getWrappedWebElement());
    }

    public void switchToDefaultFrame() {
        Log.info("Switch to default frame " + by);
        Browser.getBrowser().switchToDefaultFame();
    }

    public void typeFile(String path) {
        Log.info("Type file " + by);
        getWrappedWebElement().findElement(by).sendKeys(new File(path).getAbsolutePath());
    }

    @Override
    public String toString() {
        return "element: " + by;
    }
}
