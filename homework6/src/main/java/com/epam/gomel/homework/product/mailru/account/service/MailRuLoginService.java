package com.epam.gomel.homework.product.mailru.account.service;

import com.epam.gomel.homework.product.mailru.account.exception.MailRuLoginException;
import com.epam.gomel.homework.product.mailru.account.page.MailRuLoginPage;
import com.epam.gomel.homework.product.mailru.letter.page.MailRuHeaderPage;
import com.epam.gomel.homework.product.mailru.account.bo.MailRuAccount;

/**
 * Created by Uladzimir Shabetnik on 08.10.2016.
 */
public class MailRuLoginService {

    public void login(MailRuAccount mailRuAccount) {
        unsafeLogin(mailRuAccount);
        checkLoginSuccess();
    }

    public void unsafeLogin(MailRuAccount mailRuAccount) {
        MailRuLoginPage mailRuLoginPage = new MailRuLoginPage();
        mailRuLoginPage.open()
                .setLogin(mailRuAccount.getUsername())
                .setPassword(mailRuAccount.getPassword())
                .signIn();
    }

    public void checkLoginSuccess() {
        if (!isLoggedSuccess()) {
            throw new MailRuLoginException("Authentication failed: " + new MailRuLoginPage().getErrorMessage());
        }
    }

    public boolean isLoggedSuccess() {
        return new MailRuHeaderPage().isLoggedIn();
    }

    public void checkErrorMessage(String expectedMessage) {
        String actualMessage = new MailRuLoginPage().getErrorMessage();
        if (!actualMessage.equals(expectedMessage)) {
            throw new MailRuLoginException(String.format(
                    "Expected message is '%s' but actual is '%s'", expectedMessage, actualMessage));
        }
    }

}
