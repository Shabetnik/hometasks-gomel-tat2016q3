package com.epam.gomel.homework.product.yandexdisk.file.exception;

import com.epam.gomel.homework.framework.exception.CommonTestRuntimeException;

/**
 * Created by Uladzimir Shabetnik on 13.10.2016.
 */
public class YaDiskFindElementException extends CommonTestRuntimeException {
    public YaDiskFindElementException(String message) {
        super(message);
    }
}
