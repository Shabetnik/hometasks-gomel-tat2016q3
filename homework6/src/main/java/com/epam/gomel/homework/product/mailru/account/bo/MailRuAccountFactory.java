package com.epam.gomel.homework.product.mailru.account.bo;

import com.epam.gomel.homework.framework.utils.Randoms;
import com.epam.gomel.homework.product.mailru.letter.MailRuGlobalParameters;

/**
 * Created by Uladzimir Shabetnik on 08.10.2016.
 */
public class MailRuAccountFactory {

    public static MailRuAccount createCorrectDataAccount() {
        MailRuAccount mailRuAccount = new MailRuAccount();
        mailRuAccount.setUsername(MailRuGlobalParameters.DEFAULT_ACCOUNT_USERNAME);
        mailRuAccount.setPassword(MailRuGlobalParameters.DEFAULT_ACCOUNT_PASSWORD);
        return mailRuAccount;
    }

    public static MailRuAccount createWrongDataAccount() {
        MailRuAccount mailRuAccount = new MailRuAccount();
        mailRuAccount.setPassword(Randoms.randomAlphabetic());
        mailRuAccount.setUsername(Randoms.randomAlphabetic());
        return mailRuAccount;
    }
}
