package com.epam.gomel.homework.test.yandexdisk;

import com.epam.gomel.homework.framework.utils.FileGenerator;
import com.epam.gomel.homework.product.yandexdisk.account.bo.YaDiskAccountFactory;
import com.epam.gomel.homework.product.yandexdisk.account.service.YaDiskLoginService;
import com.epam.gomel.homework.product.yandexdisk.file.service.YaDiskRestoreFileService;
import com.epam.gomel.homework.product.yandexdisk.file.service.YaDiskTrashService;

import com.epam.gomel.homework.product.yandexdisk.file.service.YaDiskUploadFileService;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import java.io.IOException;

/**
 * Created by Uladzimir Shabetnik on 09.10.2016.
 */
public class YaDiskRestoreTest {
    private YaDiskLoginService yaDiskLoginService = new YaDiskLoginService();
    private YaDiskTrashService yaDiskTrashService = new YaDiskTrashService();
    private YaDiskRestoreFileService yaDiskRestoreFileService = new YaDiskRestoreFileService();
    private YaDiskUploadFileService yaDiskUploadFileService = new YaDiskUploadFileService();
    private FileGenerator fileGenerator = new FileGenerator();
    private String fileName;
    private String filePath;

    @BeforeClass
    public void setUpClass() throws IOException {
        fileGenerator.generateFile();
        fileName = fileGenerator.getFileName();
        filePath = fileGenerator.getFilePath();
    }

    @BeforeMethod
    public void setUp() {
        yaDiskLoginService.login(YaDiskAccountFactory.createCorrectDataAccount());
    }

    @Test(description = "Remove file to trash section")
    public void removeFileToTrashTest() throws IOException {
        yaDiskUploadFileService.uploadFile(fileName, filePath);
        yaDiskUploadFileService.checkFileUploaded(fileName);
        yaDiskUploadFileService.cleanGeneratedFiles();
        yaDiskTrashService.moveFileToTrash(fileName);
        yaDiskTrashService.checkFileInTrash(fileName);
    }

    @Test(description = "Restore file from trash section", dependsOnMethods = "removeFileToTrashTest")
    public void restoreFileToTrashTest() {
        yaDiskRestoreFileService.restoreFile(fileName);
        yaDiskRestoreFileService.checkFileRestoreToFilesSection(fileName);
    }

}
