package com.epam.gomel.homework.product.mailru.letter.bo;

import com.epam.gomel.homework.product.mailru.letter.MailRuGlobalParameters;
import com.epam.gomel.homework.framework.utils.Randoms;

/**
 * Created by Uladzimir Shabetnik on 08.10.2016.
 */
public class MailRuLetterFactory {

    public static MailRuLetter createMailWithAllFields() {
        MailRuLetter mailRuLetter = new MailRuLetter();
        mailRuLetter.setAddress(MailRuGlobalParameters.DEFAULT_ADDRESS_MAIL);
        mailRuLetter.setSubject(Randoms.randomAlphabetic());
        mailRuLetter.setText(Randoms.randomAlphabetic());
        return mailRuLetter;

    }

    public static MailRuLetter createMailWithoutAddress() {
        MailRuLetter mailRuLetter = new MailRuLetter();
        mailRuLetter.setSubject(Randoms.randomAlphabetic());
        mailRuLetter.setText(Randoms.randomAlphabetic());
        return mailRuLetter;
    }

    public static MailRuLetter createMailWithoutSubject() {
        MailRuLetter mailRuLetter = new MailRuLetter();
        mailRuLetter.setAddress(MailRuGlobalParameters.DEFAULT_ADDRESS_MAIL);
        mailRuLetter.setText(Randoms.randomAlphabetic());
        return mailRuLetter;
    }

}
