package com.epam.gomel.homework.product.yandexdisk.file.service;

import com.epam.gomel.homework.product.yandexdisk.file.exception.YaDiskFailedOperationException;
import com.epam.gomel.homework.product.yandexdisk.file.page.YaDiskContentPage;
import com.epam.gomel.homework.product.yandexdisk.file.page.YaDiskLeftMenuPage;
import com.epam.gomel.homework.product.yandexdisk.file.page.YaDiskRightMenuPage;
import org.openqa.selenium.NoSuchElementException;

/**
 * Created by Uladzimir Shabetnik on 09.10.2016.
 */
public class YaDiskRestoreFileService {
    private YaDiskContentPage yaDiskContentPage = new YaDiskContentPage();
    private YaDiskRightMenuPage yaDiskRightMenuPage = new YaDiskRightMenuPage();
    private YaDiskLeftMenuPage yaDiskLeftMenuPage = new YaDiskLeftMenuPage();

    public void restoreFile(String fileName) {
        yaDiskContentPage
                .enterToTrash()
                .findFile(fileName)
                .choiceFile();
        yaDiskRightMenuPage.restoreFile();
    }

    public void checkFileRestoreToFilesSection(String fileName) {
        yaDiskLeftMenuPage.enterToFilesSection();
        try {
            yaDiskContentPage.findFile(fileName);
        } catch (NoSuchElementException e) {
            throw new YaDiskFailedOperationException("File was not remove to trash section");
        }
    }
}
