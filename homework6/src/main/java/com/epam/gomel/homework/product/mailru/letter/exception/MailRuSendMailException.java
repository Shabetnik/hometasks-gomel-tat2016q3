package com.epam.gomel.homework.product.mailru.letter.exception;

import com.epam.gomel.homework.framework.exception.CommonTestRuntimeException;

/**
 * Created by Uladzimir Shabetnik on 08.10.2016.
 */
public class MailRuSendMailException extends CommonTestRuntimeException {

    public MailRuSendMailException(String message) {
        super(message);
    }
}
