package com.epam.gomel.homework.product.yandexdisk.file.service;

import com.epam.gomel.homework.framework.utils.FileCheckerTool;
import com.epam.gomel.homework.product.yandexdisk.file.exception.YaDiskUploadFileException;
import com.epam.gomel.homework.product.yandexdisk.file.page.YaDiskContentPage;
import com.epam.gomel.homework.product.yandexdisk.file.page.YaDiskHeaderPage;
import org.openqa.selenium.NoSuchElementException;

import java.io.IOException;

/**
 * Created by Uladzimir Shabetnik on 09.10.2016.
 */
public class YaDiskUploadFileService {
    private FileCheckerTool fileCheckerTool = new FileCheckerTool();

    public void uploadFile(String fileName, String filePath) {
        YaDiskHeaderPage yaDiskHeaderPage = new YaDiskHeaderPage();
        yaDiskHeaderPage
                .uploadFile(fileName, filePath)
                .waitForUploadingFile(fileName)
                .closeDialogUploadingFile();
    }

    public void checkFileUploaded(String fileName) {
        YaDiskContentPage yaDiskContentPage = new YaDiskContentPage();
        try {
            yaDiskContentPage.findFile(fileName);
        } catch (NoSuchElementException e) {
            throw new YaDiskUploadFileException("File not found on yandex disk");
        }
    }

    public void cleanGeneratedFiles() throws IOException {
        fileCheckerTool.deleteGenerateFilesDirectory();
    }

}
