package com.epam.gomel.homework.framework.runner;

import org.testng.TestNG;
import java.util.Arrays;
import java.util.List;

/**
 * Created by Uladzimir Shabetnik on 08.10.2016.
 */
public class RunnerMailRu {
    public static void main(String[] args) {
        TestNG testNG = new TestNG();
        List<String> files = Arrays.asList("./src/main/resources/testsuites/mailrutestsuite.xml");
        testNG.setTestSuites(files);
        testNG.run();
    }
}
