package com.epam.gomel.homework.test.mailru;

import com.epam.gomel.homework.product.mailru.account.bo.MailRuAccountFactory;
import com.epam.gomel.homework.product.mailru.letter.bo.MailRuLetter;
import com.epam.gomel.homework.product.mailru.letter.service.MailRuFindLetterService;
import com.epam.gomel.homework.product.mailru.letter.service.MailRuSaveLetterService;
import org.testng.Assert;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;
import com.epam.gomel.homework.product.mailru.account.service.MailRuLoginService;
import com.epam.gomel.homework.product.mailru.letter.bo.MailRuLetterFactory;
import com.epam.gomel.homework.product.mailru.letter.service.MailRuDeleteLetterService;

/**
 * Created by Uladzimir Shabetnik on 09.10.2016.
 */
public class MailRuDraftTest {
    private MailRuLoginService mailRuLoginService = new MailRuLoginService();
    private MailRuSaveLetterService mailRuSaveLetterService = new MailRuSaveLetterService();
    private MailRuFindLetterService mailRuFindLetterService = new MailRuFindLetterService();
    private MailRuDeleteLetterService deleteMailInDrafts = new MailRuDeleteLetterService();
    private String subjectForMail;

    @BeforeMethod
    public void setUp() {
        mailRuLoginService.login(MailRuAccountFactory.createCorrectDataAccount());
    }

    @Test(description = "Must save a letter to 'Draft' section")
    public void saveMailInDraftSectionTest() {
        MailRuLetter mailRuLetter = MailRuLetterFactory.createMailWithAllFields();
        Boolean actualResult = mailRuSaveLetterService.isMailSavedInDrafts(mailRuLetter);
        subjectForMail = mailRuLetter.getSubject();
        Assert.assertTrue(actualResult, "saveMailInDraftSectionTest error");
    }

    @Test(description = "Must check a letter with subject in 'drafts' section",
            dependsOnMethods = "saveMailInDraftSectionTest")
    public void isMailInDraftsSectionTest() {
        Boolean actualResult = mailRuFindLetterService.isMailFoundInDraftsBySubject(subjectForMail);
        Assert.assertTrue(actualResult, "isMailWithInDraftsSectionTest error");
    }

    @Test(description = "Must delete mail with subject from draft section",
            dependsOnMethods = "isMailInDraftsSectionTest")
    public void deleteMailInDraftsSectionWithSubjectTest() {
        deleteMailInDrafts.deleteNeededMailFromDrafts(subjectForMail);
        deleteMailInDrafts.checkMailDeletedFromDrafts(subjectForMail);
    }

    @Test(description = "Must check mail in 'trash' section",
            dependsOnMethods = "deleteMailInDraftsSectionWithSubjectTest")
    public void isMailInTrashSectionTest() {
        Boolean actualResult = mailRuFindLetterService.isMailFoundInTrashBySubject(subjectForMail);
        Assert.assertTrue(actualResult, "isMailInTrashSectionTest error");
    }

    @Test(description = "Must delete all mail letters permanently from trash section",
            dependsOnMethods = "isMailInTrashSectionTest")
    public void deleteAllMailPermanentlyTrashSectionTest() {
        deleteMailInDrafts.deleteSelectMailFromTrash(subjectForMail);
        deleteMailInDrafts.checkMailDeletedFromTrash(subjectForMail);
    }
}
