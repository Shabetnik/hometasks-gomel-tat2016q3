package com.epam.gomel.homework.framework.ui.browser;

import com.epam.gomel.homework.framework.parser.JCommanderParameter;
import com.epam.gomel.homework.framework.ui.browser.properties.ChromeProperties;
import com.epam.gomel.homework.framework.ui.browser.properties.FirefoxProperties;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.RemoteWebDriver;

import java.net.MalformedURLException;
import java.net.URL;

/**
 * Created by Uladzimir Shabetnik on 15.10.2016.
 */
public class WebDriverFactory {
    private static final String URL_FOR_REMOTE = "http://127.0.0.1:4444/wd/hub";

    public static RemoteWebDriver getWebDriver() throws MalformedURLException {
        RemoteWebDriver remoteWebDriver;
        String url = "http://" + JCommanderParameter.instance().getHost() + ":"
                + JCommanderParameter.instance().getPort()
                + "/wd/hub";

        switch (JCommanderParameter.instance().getBrowserType()) {
            case CHROME:
                remoteWebDriver = new RemoteWebDriver(new URL(url), ChromeProperties.getChromeProperties());
                System.setProperty("webdriver.chrome.driver", JCommanderParameter.instance().getChromeDriverPath());
                break;
            case FIREFOX:
                DesiredCapabilities dcFox = DesiredCapabilities.firefox();
                dcFox.setCapability(FirefoxDriver.PROFILE, FirefoxProperties.getFirefoxProperties());
                remoteWebDriver = new RemoteWebDriver(new URL(url), dcFox);
                break;
            default:
                throw new RuntimeException("No support:" + JCommanderParameter.instance().getBrowserType());
        }
        return remoteWebDriver;
    }
}
