package com.epam.gomel.homework.product.yandexdisk.account.bo;

import lombok.Getter;
import lombok.Setter;

/**
 * Created by Uladzimir Shabetnik on 08.10.2016.
 */
public class YaDiskAccount {
    @Getter @Setter
    private String username;
    @Getter @Setter
    private String password;

}
