package com.epam.gomel.homework.product.yandexdisk.file.exception;

import com.epam.gomel.homework.framework.exception.CommonTestRuntimeException;

/**
 * Created by Uladzimir Shabetnik on 09.10.2016.
 */
public class YaDiskUploadFileException extends CommonTestRuntimeException {

    public YaDiskUploadFileException(String message) {
        super(message);
    }
}
