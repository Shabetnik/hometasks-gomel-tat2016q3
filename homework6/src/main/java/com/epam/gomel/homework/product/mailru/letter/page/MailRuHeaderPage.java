package com.epam.gomel.homework.product.mailru.letter.page;

import com.epam.gomel.homework.framework.ui.element.Element;
import org.openqa.selenium.By;

/**
 * Created by Uladzimir Shabetnik on 08.10.2016.
 */
public class MailRuHeaderPage {
    private static final By LOGIN_SUCCESS_LOCATOR = By.xpath("//i[@id='PH_user-email']");
    Element loginBar = new Element(LOGIN_SUCCESS_LOCATOR);

    public boolean isLoggedIn() {
        return loginBar.isVisible();
    }

}
