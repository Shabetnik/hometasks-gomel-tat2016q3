package com.epam.gomel.homework.framework.parser;

import com.beust.jcommander.IStringConverter;
import com.beust.jcommander.Parameter;
import com.epam.gomel.homework.framework.ui.browser.BrowserType;
import lombok.Getter;

/**
 * Created by Uladzimir Shabetnik on 15.10.2016.
 */
public class JCommanderParameter {
    private static JCommanderParameter instance;

    @Parameter(description = "How to use", names = "--help", help = true)
    @Getter
    private boolean help;

    @Parameter(description = "Need choose browser type, in case browser you need enter(-cp) path to chromedriver.exe",
            names = {"--browser", "-b"},
            converter = BrowserTypeConverter.class)
    @Getter
    private BrowserType browserType = BrowserType.FIREFOX;

    @Parameter(description = "Path to chrome driver", names = {"--chromepath", "-cp"})
    @Getter
    private String chromeDriverPath = "./src/main/resources/drivers/chromedriver.exe";

    @Parameter(description = "Selenium host for remote webdriver:", names = {"--host", "-h"}, required = true)
    @Getter
    private String host;

    @Parameter(description = "Selenium port for remote webdriver", names = {"--port", "-p"}, required = true)
    @Getter
    private String port;

    @Parameter(description = "Path to xml sute file", names = {"--suite", "-st"}, required = true)
    @Getter
    private String xmlSutePath;

    public static JCommanderParameter instance() {
        if (instance == null) {
            instance = new JCommanderParameter();
        }
        return instance;
    }

    public static class BrowserTypeConverter implements IStringConverter<BrowserType> {
        @Override
        public BrowserType convert(String s) {
            return BrowserType.valueOf(s.toUpperCase());
        }
    }

}
