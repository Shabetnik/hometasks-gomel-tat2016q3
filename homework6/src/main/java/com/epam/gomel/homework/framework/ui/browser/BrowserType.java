package com.epam.gomel.homework.framework.ui.browser;

/**
 * Created by Uladzimir Shabetnik on 15.10.2016.
 */
public enum BrowserType {
    FIREFOX, CHROME
}

