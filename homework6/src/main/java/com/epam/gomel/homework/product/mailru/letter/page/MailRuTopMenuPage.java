package com.epam.gomel.homework.product.mailru.letter.page;

import com.epam.gomel.homework.framework.ui.element.ActionWrapper;
import com.epam.gomel.homework.framework.ui.element.Element;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;

/**
 * Created by Uladzimir Shabetnik on 08.10.2016.
 */
public class MailRuTopMenuPage {
    private static final By WRITE_MAIL_BUTTON_LOCATOR
            = By.xpath("//a[@class='b-toolbar__btn js-shortcut']");
    private static final By SEND_MAIL_BUTTON_LOCATOR = By.xpath("//div[@data-name='send']");
    private static final By SAVE_DRAFT_BUTTON_LOCATOR = By.xpath("//div[@data-name='saveDraft']");
    private Element writeMailButton = new Element(WRITE_MAIL_BUTTON_LOCATOR);
    private Element sendMailButton = new Element(SEND_MAIL_BUTTON_LOCATOR);
    private Element saveToDraftButton = new Element(SAVE_DRAFT_BUTTON_LOCATOR);

    public void openToWriteMail() {
        writeMailButton.waitForVisible();
        writeMailButton.click();
    }

    public void sendMail() {
        sendMailButton.waitForVisible();
        sendMailButton.click();
    }

    public void saveInDraftMail() {
        saveToDraftButton.click();
    }

    public void deleteSelectedMail() {
        ActionWrapper actionWrapper = new ActionWrapper();
        actionWrapper.getActionOnElement()
                .sendKeys(Keys.DELETE)
                .build()
                .perform();
    }

}


