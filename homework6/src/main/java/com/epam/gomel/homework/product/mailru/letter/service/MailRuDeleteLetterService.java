package com.epam.gomel.homework.product.mailru.letter.service;

import com.epam.gomel.homework.product.mailru.letter.exception.MailRuDeleteException;
import com.epam.gomel.homework.product.mailru.letter.page.MailRuContentPage;
import com.epam.gomel.homework.product.mailru.letter.page.MailRuLeftMenuPage;
import org.openqa.selenium.TimeoutException;
import com.epam.gomel.homework.product.mailru.letter.page.MailRuTopMenuPage;

/**
 * Created by Uladzimir Shabetnik on 08.10.2016.
 */
public class MailRuDeleteLetterService {
    MailRuContentPage mailRuContentPage = new MailRuContentPage();
    MailRuLeftMenuPage mailRuLeftMenuPage = new MailRuLeftMenuPage();
    MailRuTopMenuPage mailRuTopMenuPage = new MailRuTopMenuPage();

    public void deleteNeededMailFromDrafts(String subject) {
        mailRuLeftMenuPage.enterToDraftsMail();
        mailRuContentPage.clickNeededMailCheckbox(subject);
        mailRuTopMenuPage.deleteSelectedMail();
    }

    public void deleteAllMailInTrash() {
        mailRuLeftMenuPage.cleanAllMailInTrash();
    }

    public void deleteSelectMailFromTrash(String subject) {
        mailRuLeftMenuPage.enterToTrashMail();
        mailRuContentPage.clickNeededMailCheckbox(subject);
        mailRuTopMenuPage.deleteSelectedMail();
    }

    public void checkMailDeletedFromTrash(String subject) {
        try {
            mailRuLeftMenuPage.enterToTrashMail();
            mailRuContentPage.waitDisappearNeededMailBySubject(subject);
        } catch (TimeoutException e) {
            throw new MailRuDeleteException("MailRuLetter delete failed");
        }
    }

    public void checkMailDeletedFromDrafts(String subject) {
        try {
            mailRuLeftMenuPage.enterToDraftsMail();
            mailRuContentPage.waitDisappearNeededMailBySubject(subject);
        } catch (TimeoutException e) {
            throw new MailRuDeleteException("MailRuLetter delete failed");
        }
    }
}
