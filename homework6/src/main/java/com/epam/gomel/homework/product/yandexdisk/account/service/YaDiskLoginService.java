package com.epam.gomel.homework.product.yandexdisk.account.service;

import com.epam.gomel.homework.product.yandexdisk.account.bo.YaDiskAccount;
import com.epam.gomel.homework.product.yandexdisk.account.exception.YaDiskLoginException;
import com.epam.gomel.homework.product.yandexdisk.account.page.YaDiskLoginPage;
import com.epam.gomel.homework.product.yandexdisk.file.page.YaDiskHeaderPage;

/**
 * Created by Uladzimir Shabetnik on 08.10.2016.
 */
public class YaDiskLoginService {

    public void login(YaDiskAccount yaDiskAccount) {
        unsafeLogin(yaDiskAccount);
        checkLoginSuccess();
    }

    public void unsafeLogin(YaDiskAccount yaDiskAccount) {
        YaDiskLoginPage yaDiskLoginPage = new YaDiskLoginPage();
        yaDiskLoginPage.open()
                .setLogin(yaDiskAccount.getUsername())
                .setPassword(yaDiskAccount.getPassword())
                .signIn();
    }

    public void checkLoginSuccess() {
        if (!isLoggedSuccess()) {
            throw new YaDiskLoginException("Authentication failed: ");
        }
    }

    public boolean isLoggedSuccess() {
        return new YaDiskHeaderPage().isLoggedIn();
    }

    public boolean checkFailedLogin(boolean expectedResult) {
        boolean actualResult = new YaDiskLoginPage().checkFailedLogin();
        if (!actualResult == expectedResult) {
            throw new YaDiskLoginException(String.format(
                    "Expected result is '%s' but actual is '%s'", expectedResult, actualResult));
        }
        return actualResult;
    }

}
