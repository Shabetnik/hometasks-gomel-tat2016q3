package com.epam.gomel.homework.framework.ui.element;

import com.epam.gomel.homework.framework.ui.browser.Browser;
import org.openqa.selenium.Alert;

/**
 * Created by Uladzimir Shabetnik on 09.10.2016.
 */
public class AlertWrapper {

    public void acceptAlert() {
        Alert alert = Browser.getBrowser().getWrappedDriver().switchTo().alert();
        alert.accept();
    }

    public String getAlertText() {
        Alert alert = Browser.getBrowser().getWrappedDriver().switchTo().alert();
        return alert.getText();
    }
}
