package com.epam.gomel.homework.product.yandexdisk.file.service;

import com.epam.gomel.homework.framework.utils.FileCheckerTool;
import com.epam.gomel.homework.product.yandexdisk.file.exception.YaDiskUploadFileException;
import com.epam.gomel.homework.product.yandexdisk.file.page.YaDiskContentPage;
import com.epam.gomel.homework.product.yandexdisk.file.page.YaDiskRightMenuPage;

import java.io.IOException;

/**
 * Created by Uladzimir Shabetnik on 09.10.2016.
 */
public class YaDiskDownloadFileService {

    private FileCheckerTool fileCheckerTool = new FileCheckerTool();

    public void downloadFile(String fileName) {
        YaDiskContentPage yaDiskContentPage = new YaDiskContentPage();
        YaDiskRightMenuPage yaDiskRightMenuPage = new YaDiskRightMenuPage();
        yaDiskContentPage.findFile(fileName);
        yaDiskContentPage.choiceFile();
        yaDiskRightMenuPage.downloadFile();
        fileCheckerTool.waitForFile(fileName);
    }

    public void checkFileContent(String fileName) throws IOException {
        Boolean expectedResult = true;
        if (!expectedResult == fileCheckerTool.isContentFileSame(fileName)) {
            throw new YaDiskUploadFileException("Files have different content");
        }
    }

    public void deleteDownloadDirectory() throws IOException {
        fileCheckerTool.deleteDownloadDirectory();
    }

}
