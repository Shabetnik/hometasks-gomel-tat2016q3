package com.epam.gomel.homework.framework.runner;

import com.epam.gomel.homework.framework.parser.JCommanderParameter;
import com.epam.gomel.homework.framework.parser.JCommanderParser;
import org.testng.TestNG;
import java.util.Arrays;
import java.util.List;

/**
 * Created by Uladzimir Shabetnik on 15.10.2016.
 */
public class TestRunner {
    public static void main(String[] args) {
        TestNG testNG = new TestNG();
        JCommanderParser.parseCli(args);
        List<String> files = Arrays.asList(JCommanderParameter.instance().getXmlSutePath());
        testNG.setTestSuites(files);
        testNG.run();
    }
}
