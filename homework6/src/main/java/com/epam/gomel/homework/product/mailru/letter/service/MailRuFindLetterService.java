package com.epam.gomel.homework.product.mailru.letter.service;

import com.epam.gomel.homework.product.mailru.letter.page.MailRuContentPage;
import com.epam.gomel.homework.product.mailru.letter.page.MailRuLeftMenuPage;

/**
 * Created by Uladzimir Shabetnik on 09.10.2016.
 */
public class MailRuFindLetterService {
    MailRuContentPage mailRuContentPage = new MailRuContentPage();
    MailRuLeftMenuPage mailRuLeftMenuPage = new MailRuLeftMenuPage();

    public boolean isMailFoundInInboxBySubject(String subject) {
        mailRuLeftMenuPage.enterToInboxMail();
        return mailRuContentPage.findNeededMailBySubject(subject);
    }

    public boolean isMailFoundInSentBySubject(String subject) {
        mailRuLeftMenuPage.enterToSentMail();
        return mailRuContentPage.findNeededMailBySubject(subject);
    }

    public boolean isMailFoundInDraftsBySubject(String subject) {
        mailRuLeftMenuPage.enterToDraftsMail();
        return mailRuContentPage.findNeededMailBySubject(subject);
    }

    public boolean isMailFoundInTrashBySubject(String subject) {
        mailRuLeftMenuPage.enterToTrashMail();
        return mailRuContentPage.findNeededMailBySubject(subject);
    }

    public boolean isMailFoundInInboxWithoutSubject() {
        mailRuLeftMenuPage.enterToInboxMail();
        return mailRuContentPage.findNeededMailWithoutSubject();
    }

    public boolean isMailFoundInSentWithoutSubject() {
        mailRuLeftMenuPage.enterToSentMail();
        return mailRuContentPage.findNeededMailWithoutSubject();
    }

}
