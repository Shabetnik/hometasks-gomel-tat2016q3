package com.epam.gomel.homework.test.yandexdisk;

import com.epam.gomel.homework.framework.utils.FileGenerator;
import com.epam.gomel.homework.product.yandexdisk.account.bo.YaDiskAccountFactory;
import com.epam.gomel.homework.product.yandexdisk.account.service.YaDiskLoginService;
import com.epam.gomel.homework.product.yandexdisk.file.service.YaDiskDownloadFileService;
import com.epam.gomel.homework.product.yandexdisk.file.service.YaDiskUploadFileService;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import java.io.IOException;

/**
 * Created by Uladzimir Shabetnik on 09.10.2016.
 */
public class YaDiskUploadDownloadFileTest {
    private YaDiskLoginService yaDiskLoginService = new YaDiskLoginService();
    private YaDiskUploadFileService yaDiskUploadFileService = new YaDiskUploadFileService();
    private YaDiskDownloadFileService yaDiskDownloadFileService = new YaDiskDownloadFileService();
    private FileGenerator fileGenerator = new FileGenerator();
    private String fileName;
    private String filePath;

    @BeforeClass
    public void setUpClass() throws IOException {
        fileGenerator.generateFile();
        fileName = fileGenerator.getFileName();
        filePath = fileGenerator.getFilePath();
    }

    @BeforeMethod
    public void setUp() {
        yaDiskLoginService.login(YaDiskAccountFactory.createCorrectDataAccount());
    }

    @Test(description = "Must upload file to 'disk.yandex.ru' and check it")
    public void uploadTest() {
        yaDiskUploadFileService.uploadFile(fileName, filePath);
        yaDiskUploadFileService.checkFileUploaded(fileName);
    }

    @Test(description = "Must download file", dependsOnMethods = "uploadTest")
    public void downloadTest() throws IOException {
        yaDiskDownloadFileService.downloadFile(fileName);
        yaDiskDownloadFileService.checkFileContent(fileName);
        yaDiskDownloadFileService.deleteDownloadDirectory();
        yaDiskUploadFileService.cleanGeneratedFiles();
    }

}
