package com.epam.gomel.homework.framework.utils;

import org.apache.commons.lang3.RandomStringUtils;
import org.apache.commons.lang3.RandomUtils;

import java.util.UUID;

/**
 * Created by Uladzimir Shabetnik on 08.10.2016.
 */
public class Randoms {
    public static final int DEFAULT_LENGTH = 10;

    public static String randomAlphanumeric() {
        return RandomStringUtils.randomAlphanumeric(DEFAULT_LENGTH);
    }

    public static String randomAlphabetic() {
        return RandomStringUtils.randomAlphabetic(DEFAULT_LENGTH);
    }

    public static String randomNumeric() {
        return RandomStringUtils.randomNumeric(DEFAULT_LENGTH);
    }

    public static int randomInteger() {
        return RandomUtils.nextInt(Integer.MIN_VALUE, Integer.MAX_VALUE);
    }

    public UUID randomUUID() {
        return UUID.randomUUID();
    }

}
