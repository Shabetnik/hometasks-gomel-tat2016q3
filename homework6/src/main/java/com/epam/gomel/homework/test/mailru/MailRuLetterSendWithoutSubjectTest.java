package com.epam.gomel.homework.test.mailru;

import com.epam.gomel.homework.product.mailru.account.bo.MailRuAccountFactory;
import com.epam.gomel.homework.product.mailru.account.service.MailRuLoginService;
import com.epam.gomel.homework.product.mailru.letter.bo.MailRuLetter;
import com.epam.gomel.homework.product.mailru.letter.bo.MailRuLetterFactory;
import com.epam.gomel.homework.product.mailru.letter.service.MailRuFindLetterService;
import com.epam.gomel.homework.product.mailru.letter.service.MailRuSendLetterService;
import org.testng.Assert;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

/**
 * Created by Uladzimir Shabetnik on 09.10.2016.
 */
public class MailRuLetterSendWithoutSubjectTest {
    private static final String EXPECTED_SUCCESS_SENT_MAIL_MESSAGE = "Ваше письмо отправлено. Перейти во Входящие";
    private MailRuLoginService mailRuLoginService = new MailRuLoginService();
    private MailRuSendLetterService mailRuSendLetterService = new MailRuSendLetterService();
    private MailRuFindLetterService mailRuFindLetterService = new MailRuFindLetterService();

    @BeforeMethod
    public void setUp() {
        mailRuLoginService.login(MailRuAccountFactory.createCorrectDataAccount());
    }

    @Test(description = "Must send a letter without subject and go to page with success sent mail message")
    public void sentMailWithoutSubjectTest() throws InterruptedException {
        MailRuLetter mailRuLetter = MailRuLetterFactory.createMailWithoutSubject();
        mailRuSendLetterService.sendMailWithoutSubject(mailRuLetter);
        mailRuSendLetterService.checkSuccessSentMailMessage(EXPECTED_SUCCESS_SENT_MAIL_MESSAGE);
    }

    @Test(description = "Must check a letter with no subject in 'inbox' section",
            dependsOnMethods = "sentMailWithoutSubjectTest")
    public void isSentMailWithoutSubjectInInboxSectionTest() {
        Boolean actualResult = mailRuFindLetterService.isMailFoundInInboxWithoutSubject();
        Assert.assertTrue(actualResult, "isSentMailWithSubjectInInboxSectionTest error");
    }

    @Test(description = "Must check a letter with no subject in 'sent' section",
            dependsOnMethods = "isSentMailWithoutSubjectInInboxSectionTest")
    public void isSentMailWithoutSubjectInSentSectionTest() {
        Boolean actualResult = mailRuFindLetterService.isMailFoundInSentWithoutSubject();
        Assert.assertTrue(actualResult, "isSentMailWithSubjectInSentSectionTest error");
    }

}
