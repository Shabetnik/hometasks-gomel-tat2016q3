package com.epam.gomel.homework.product.yandexdisk.account.bo;

import com.epam.gomel.homework.framework.utils.Randoms;
import com.epam.gomel.homework.product.yandexdisk.file.YaDiskGlobalParameters;

/**
 * Created by Uladzimir Shabetnik on 08.10.2016.
 */
public class YaDiskAccountFactory {

    public static YaDiskAccount createCorrectDataAccount() {
        YaDiskAccount yaDiskAccount = new YaDiskAccount();
        yaDiskAccount.setUsername(YaDiskGlobalParameters.DEFAULT_ACCOUNT_USERNAME);
        yaDiskAccount.setPassword(YaDiskGlobalParameters.DEFAULT_ACCOUNT_PASSWORD);
        return yaDiskAccount;
    }

    public static YaDiskAccount createWrongDataAccount() {
        YaDiskAccount yaDiskAccount = new YaDiskAccount();
        yaDiskAccount.setPassword(Randoms.randomAlphabetic());
        yaDiskAccount.setUsername(Randoms.randomAlphabetic());
        return yaDiskAccount;
    }
}
