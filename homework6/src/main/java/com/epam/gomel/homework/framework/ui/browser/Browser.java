package com.epam.gomel.homework.framework.ui.browser;

import com.epam.gomel.homework.framework.exception.CommonTestRuntimeException;
import com.epam.gomel.homework.framework.logging.Log;
import org.apache.commons.io.FileUtils;
import org.openqa.selenium.By;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.internal.WrapsDriver;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import java.io.File;
import java.io.IOException;
import java.net.MalformedURLException;

/**
 * Created by Uladzimir Shabetnik on 08.10.2016.
 */
public final class Browser implements WrapsDriver {
    public static final int ELEMENT_WAIT_TIMEOUT_SECONDS = 60;
    private static ThreadLocal<Browser> instance = new ThreadLocal();
    private RemoteWebDriver webDriver;

    private Browser() {
        try {
            webDriver = WebDriverFactory.getWebDriver();
        } catch (MalformedURLException e) {
            throw new RuntimeException("Wrong local host");
        }
        webDriver.manage().window().maximize();
    }

    public static synchronized Browser getBrowser() {
        if (instance.get() == null) {
            instance.set(new Browser());
        }
        return instance.get();
    }

    public void close() {
        try {
            if (getWrappedDriver() != null) {
                getWrappedDriver().quit();
            }
        } finally {
            instance.set(null);
        }
    }

    @Override
    public WebDriver getWrappedDriver() {
        return webDriver;
    }

    public void open(String url) {
        Log.info("Open url: " + url);
        webDriver.get(url);
    }

    public WebElement findElement(By by) {
        return webDriver.findElement(by);
    }

    public Actions getActions() {
        return new Actions(Browser.getBrowser().getWrappedDriver());
    }

    public boolean isElementVisible(By by) {
        return findElement(by).isDisplayed();
    }

    public void waitForElementIsInvisible(By locator) {
        Log.info("Waiting until element will invisible: " + locator);
        WebDriverWait wait = new WebDriverWait(webDriver, ELEMENT_WAIT_TIMEOUT_SECONDS);
        wait.until(ExpectedConditions.invisibilityOfElementLocated(locator));
    }

    public void waitForElementIsVisible(By locator) {
        Log.info("Waiting until element will visible: " + locator);
        WebDriverWait wait = new WebDriverWait(webDriver, ELEMENT_WAIT_TIMEOUT_SECONDS);
        wait.until(ExpectedConditions.visibilityOfElementLocated(locator));
    }

    public void switchToFrameElement(WebElement frame) {
        webDriver.switchTo().frame(frame);
    }

    public void switchToDefaultFame() {
        webDriver.switchTo().defaultContent();
    }

    public void waitForElementIsAppear(By locator) {
        Log.info("Waiting until element appear: " + locator);
        WebDriverWait wait = new WebDriverWait(webDriver, ELEMENT_WAIT_TIMEOUT_SECONDS);
        wait.until(ExpectedConditions.presenceOfElementLocated(locator));
    }

    public byte[] screenshot() {
        if (instance == null) {
            throw new CommonTestRuntimeException("Screenshot failed, browser is null");
        }
        File screenshotFile = new File("logs/screenshots/" + System.nanoTime() + ".png");
        try {
            byte[] screenshotBytes = webDriver.getScreenshotAs(OutputType.BYTES);
            FileUtils.writeByteArrayToFile(screenshotFile, screenshotBytes);
            Log.info("Screenshot taken: file://" + screenshotFile.getAbsolutePath());
            return screenshotBytes;
        } catch (IOException e) {
            throw new CommonTestRuntimeException("Screenshot failed", e);
        }
    }
}
