package com.epam.gomel.homework.framework.parser;

import com.beust.jcommander.JCommander;
import com.beust.jcommander.ParameterException;
import com.epam.gomel.homework.framework.logging.Log;

/**
 * Created by Uladzimir Shabetnik on 15.10.2016.
 */
public class JCommanderParser {

    public static void parseCli(String[] args) {
        JCommander jCommander = new JCommander(JCommanderParameter.instance());
        try {
            jCommander.parse(args);
        } catch (ParameterException e) {
            Log.error(e.getMessage(), e);
            System.exit(1);
        }
        if (JCommanderParameter.instance().isHelp()) {
            jCommander.usage();
            System.exit(0);
        }
    }
}
