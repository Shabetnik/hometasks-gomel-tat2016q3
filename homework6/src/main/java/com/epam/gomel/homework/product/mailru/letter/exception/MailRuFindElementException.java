package com.epam.gomel.homework.product.mailru.letter.exception;

import com.epam.gomel.homework.framework.exception.CommonTestRuntimeException;

/**
 * Created by Uladzimir Shabetnik on 13.10.2016.
 */
public class MailRuFindElementException extends CommonTestRuntimeException {
    public MailRuFindElementException(String message) {
        super(message);
    }
}
