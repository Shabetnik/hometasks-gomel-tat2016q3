package com.epam.gomel.homework.product.mailru.account.exception;

import com.epam.gomel.homework.framework.exception.CommonTestRuntimeException;

/**
 * Created by Uladzimir Shabetnik on 08.10.2016.
 */
public class MailRuLoginException extends CommonTestRuntimeException {

    public MailRuLoginException(String message) {
        super(message);
    }
}
