Firefox remote
---------------------------------------------------
java -jar hometask6-jar-with-dependencies.jar --host 127.0.0.1 --port 4444 -st test-suites/mailrutestsuite.xml
---------------------------------------------------
Chrome remote
---------------------------------------------------
java -jar hometask6-jar-with-dependencies.jar --host 127.0.0.1 --port 4444 --browser chrome  -cp ./src/main/resources/drivers/chromedriver.exe -st test-suites/yandexdisktestsuite.xml
