package com.epam.gomel.homework.pages;

import lombok.Getter;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

/**
 * Created by Uladzimir Shabetnik on 29.09.2016.
 */
public class SuccessSaveInDraftPage {
    private static final By SUCCESS_DRAFT_LOCATOR = By.xpath("//div[@class='b-toolbar__message']");
    private static final By INFORMATION_SUCCESS_DRAFT_LOCATOR
            = By.xpath("//div[@class='b-toolbar__message']//span[@class='time']");
    private static final int WEB_DRIVER_WAIT_IN_SECOND = 5;
    private WebDriver webDriver;
    @Getter
    private String informationDraftMail;

    public SuccessSaveInDraftPage(WebDriver webDriver) {
        this.webDriver = webDriver;
    }

    public boolean isMailSaveInDraft() {
        WebDriverWait wait = new WebDriverWait(webDriver, WEB_DRIVER_WAIT_IN_SECOND);
        wait.until(ExpectedConditions.visibilityOf(webDriver.findElement(INFORMATION_SUCCESS_DRAFT_LOCATOR)));
        informationDraftMail = webDriver.findElement(INFORMATION_SUCCESS_DRAFT_LOCATOR).getText();
        PageFactory.initElements(webDriver, SentSuccessMessagePage.class);
        return webDriver.findElement(SUCCESS_DRAFT_LOCATOR).isDisplayed();
    }
}
