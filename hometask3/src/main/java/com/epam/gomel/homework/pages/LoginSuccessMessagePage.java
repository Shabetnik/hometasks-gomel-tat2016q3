package com.epam.gomel.homework.pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.PageFactory;

/**
 * Created by Uladzimir Shabetnik on 26.09.2016.
 */
public class LoginSuccessMessagePage {
    private static final By LOGIN_SUCCESS_LOCATOR = By.xpath("//i[@id='PH_user-email']");
    private WebDriver webDriver;

    public LoginSuccessMessagePage(WebDriver webDriver) {
        this.webDriver = webDriver;
    }

    public String getCurrentUser() {
        PageFactory.initElements(webDriver, LoginSuccessMessagePage.class);
        return webDriver.findElement(LOGIN_SUCCESS_LOCATOR).getText();
    }
}
