package com.epam.gomel.homework.pages;

import org.openqa.selenium.*;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.io.File;

/**
 * Created by Uladzimir Shabetnik on 26.09.2016.
 */
public class TopMenuPage {
    private static final By WRITE_MAIL_BUTTON_LOCATOR
            = By.xpath("//a[@class='b-toolbar__btn js-shortcut']");
    private static final By ADDRESS_SENT_LOCATOR
            = By.xpath("//textarea[@class='js-input compose__labels__input' and @data-original-name='To']");
    private static final By SUBJECT_LOCATOR = By.xpath("//input[@class='compose__header__field']");
    private static final By TEXT_MAIL_LOCATOR = By.xpath("//html/body");
    private static final By SENT_MAIL_BUTTON_LOCATOR = By.xpath("//div[@data-name='send']");
    private static final By IFRAME_LOCATOR_SENT_TEXT_LOCATOR
            = By.xpath("//iframe[contains(@id,'composeEditor_ifr')]");
    private static final By SAVE_DRAFT_BUTTON_LOCATOR = By.xpath("//div[@data-name='saveDraft']");
    private static final String CHECKBOX_MAIL_WITH_SUBJECT
            = "//a[@data-subject='%s' and not(ancestor::div[contains(@style,'display:none')]) "
            + "and not(ancestor::div[contains(@style,'display: none')])]"
            + "//div[@class='js-item-checkbox b-datalist__item__cbx']";
    private static final int WEB_DRIVER_WAIT_IN_SECOND = 5;
    private static final By ATTACH_BUTTON_LOCATOR = By.xpath("//input[@name='Filedata']");
    private static final String LOCATION_FILE = "./src/main/resources/upload.txt";
    private WebDriver webDriver;

    public TopMenuPage(WebDriver webDriver) {
        this.webDriver = webDriver;
    }

    public void waitForLoad(By locator) {
        WebDriverWait wait = new WebDriverWait(webDriver, WEB_DRIVER_WAIT_IN_SECOND);
        wait.until(ExpectedConditions.visibilityOf(webDriver.findElement(locator)));
    }

    public SentSuccessMessagePage sentMailWithAllFields(String address, String subject, String text) {
        waitForLoad(WRITE_MAIL_BUTTON_LOCATOR);
        webDriver.findElement(WRITE_MAIL_BUTTON_LOCATOR).click();
        waitForLoad(ADDRESS_SENT_LOCATOR);
        webDriver.findElement(ADDRESS_SENT_LOCATOR).sendKeys(address);
        webDriver.findElement(SUBJECT_LOCATOR).sendKeys(subject);
        WebElement iframe = webDriver.findElement(IFRAME_LOCATOR_SENT_TEXT_LOCATOR);
        webDriver.switchTo().frame(iframe);
        webDriver.findElement(TEXT_MAIL_LOCATOR).sendKeys(text);
        webDriver.switchTo().defaultContent();
        webDriver.findElement(ATTACH_BUTTON_LOCATOR).sendKeys(new File(LOCATION_FILE).getAbsolutePath());
        webDriver.findElement(SENT_MAIL_BUTTON_LOCATOR).click();
        return new SentSuccessMessagePage(webDriver);
    }

    public SentSuccessMessagePage sentMailWithoutSubject(String address) {
        waitForLoad(WRITE_MAIL_BUTTON_LOCATOR);
        webDriver.findElement(WRITE_MAIL_BUTTON_LOCATOR).click();
        waitForLoad(ADDRESS_SENT_LOCATOR);
        webDriver.findElement(ADDRESS_SENT_LOCATOR).sendKeys(address);
        webDriver.findElement(SENT_MAIL_BUTTON_LOCATOR).click();
        return new SentSuccessMessagePage(webDriver);
    }

    public String sentMailWithoutAddress() {
        waitForLoad(WRITE_MAIL_BUTTON_LOCATOR);
        webDriver.findElement(WRITE_MAIL_BUTTON_LOCATOR).click();
        waitForLoad(ADDRESS_SENT_LOCATOR);
        webDriver.findElement(SENT_MAIL_BUTTON_LOCATOR).click();
        Alert alert = webDriver.switchTo().alert();
        String alertText = alert.getText();
        alert.accept();
        return alertText;
    }

    public SuccessSaveInDraftPage saveMailInDrafts(String address, String subject, String text) {
        waitForLoad(WRITE_MAIL_BUTTON_LOCATOR);
        webDriver.findElement(WRITE_MAIL_BUTTON_LOCATOR).click();
        waitForLoad(ADDRESS_SENT_LOCATOR);
        webDriver.findElement(ADDRESS_SENT_LOCATOR).sendKeys(address);
        webDriver.findElement(SUBJECT_LOCATOR).sendKeys(subject);
        WebElement iframe = webDriver.findElement(IFRAME_LOCATOR_SENT_TEXT_LOCATOR);
        webDriver.switchTo().frame(iframe);
        webDriver.findElement(TEXT_MAIL_LOCATOR).sendKeys(text);
        webDriver.switchTo().defaultContent();
        webDriver.findElement(SAVE_DRAFT_BUTTON_LOCATOR).click();
        return new SuccessSaveInDraftPage(webDriver);
    }

    public boolean deleteMailInDrafts(String subject) {
        try {
            webDriver.findElement(By.xpath(String.format(CHECKBOX_MAIL_WITH_SUBJECT, subject))).click();
            Actions actions = new Actions(webDriver);
            actions.sendKeys(Keys.DELETE).build().perform();
            return true;
        } catch (NoSuchElementException e) {
            return false;
        }
    }
}
