package com.epam.gomel.homework.pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

/**
 * Created by Uladzimir Shabetnik on 25.09.2016.
 */
public class LoginPage {
    private static final By LOGIN_INPUT_LOCATOR = By.xpath("//input[@id='mailbox__login']");
    private static final By PASSWORD_INPUT_LOCATOR = By.xpath("//input[@id='mailbox__password']");
    private static final By BUTTON_LOGIN_LOCATOR = By.xpath("//input[@id='mailbox__auth__button']");
    private WebDriver webDriver;

    public LoginPage(WebDriver webDriver) {
        this.webDriver = webDriver;
    }

    public LoginSuccessMessagePage correctLogin(String login, String password) {
        webDriver.findElement(LOGIN_INPUT_LOCATOR).sendKeys(login);
        webDriver.findElement(PASSWORD_INPUT_LOCATOR).sendKeys(password);
        webDriver.findElement(BUTTON_LOGIN_LOCATOR).click();
        return new LoginSuccessMessagePage(webDriver);
    }

    public void login(String login, String password) {
        correctLogin(login, password);
    }

    public LoginErrorMessagePage incorrectLogin(String login, String password) {
        correctLogin(login, password);
        return new LoginErrorMessagePage(webDriver);
    }
}
