package com.epam.gomel.homework.pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.PageFactory;

/**
 * Created by Uladzimir Shabetnik on 26.09.2016.
 */
public class SentSuccessMessagePage {
    private static final By SENT_MESSAGE_SUCCES_LOCATOR = By.xpath("(//div[@class='message-sent__title'])");
    private WebDriver webDriver;

    public SentSuccessMessagePage(WebDriver webDriver) {
        this.webDriver = webDriver;
    }

    public boolean isMessageSent() {
        PageFactory.initElements(webDriver, SentSuccessMessagePage.class);
        return webDriver.findElement(SENT_MESSAGE_SUCCES_LOCATOR).isDisplayed();
    }
}
