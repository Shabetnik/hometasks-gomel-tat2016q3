package com.epam.gomel.homework.pages;

import org.openqa.selenium.By;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;


/**
 * Created by Uladzimir Shabetnik on 27.09.2016.
 */
public class LeftMenuPage {
    private static final By INBOX_MAIL_SECTION_BUTTON_LOCATOR
            = By.xpath("//div[@data-mnemo='nav-folders']//a[@href='/messages/inbox/']");
    private static final By SENT_MAIL_SECTION_BUTTON_LOCATOR
            = By.xpath("//div[@data-mnemo='nav-folders']//a[@href='/messages/sent/']");
    private static final By DRAFTS_MAIL_SECTION_BUTTON_LOCATOR
            = By.xpath("//div[@data-mnemo='nav-folders']//a[@href='/messages/drafts/']");
    private static final By TRASH_MAIL_SECTION_BUTTON_LOCATOR
            = By.xpath("//div[@data-mnemo='nav-folders']//a[@href='/messages/trash/']");
    private static final By FIND_MAIL_WITHOUT_SUBJECT_LOCATOR = By.xpath("//div[contains(text(),'Без темы') "
            + "and not(ancestor::div[contains(@style,'display:none')]) "
            + "and not(ancestor::div[contains(@style,'display: none')])]");
    private static final String SUBJECT_MAIL = "//a[@data-subject='%s' "
            + "and not(ancestor::div[contains(@style,'display:none')]) "
            + "and not(ancestor::div[contains(@style,'display: none')])]";
    private static final By CLEAN_TRASH_BUTTON_LOCATOR
            = By.xpath("//button[@class='btn btn_main btn_stylish js-clearButton']");
    private static final By CHOICE_FRAME_TRASH_CLEAN_LOCATOR
            = By.xpath("//span[@class='b-nav__link__text js-clear-text']");
    private static final By SAVE_DRAFT_SUCCES_LOCATOR = By.xpath("//div[@class='b-toolbar__message']");
    private static final int WEB_DRIVER_WAIT_IN_SECOND = 5;
    private WebDriver webDriver;

    public LeftMenuPage(WebDriver webDriver) {
        this.webDriver = webDriver;
    }

    public void waitForLoad(By locator) {
        WebDriverWait wait = new WebDriverWait(webDriver, WEB_DRIVER_WAIT_IN_SECOND);
        wait.until(ExpectedConditions.visibilityOfElementLocated(locator));

    }

    public boolean isInboxMailWithSubjectExist(String subjectMail) {
        webDriver.findElement(INBOX_MAIL_SECTION_BUTTON_LOCATOR).click();
        waitForLoad(By.xpath(String.format(SUBJECT_MAIL, subjectMail)));
        return webDriver.findElement(By.xpath(String.format(SUBJECT_MAIL, subjectMail))).isDisplayed();
    }

    public boolean isInboxMailWithoutSubjectExist() {
        webDriver.findElement(INBOX_MAIL_SECTION_BUTTON_LOCATOR).click();
        waitForLoad(INBOX_MAIL_SECTION_BUTTON_LOCATOR);
        return webDriver.findElement(FIND_MAIL_WITHOUT_SUBJECT_LOCATOR).isDisplayed();
    }

    public boolean isSentMailWithSubjectExist(String subjectMail) {
        webDriver.findElement(SENT_MAIL_SECTION_BUTTON_LOCATOR).click();
        waitForLoad(By.xpath(String.format(SUBJECT_MAIL, subjectMail)));
        return webDriver.findElement(By.xpath(String.format(SUBJECT_MAIL, subjectMail))).isDisplayed();
    }

    public boolean isSentMailWithoutSubjectExist() {
        webDriver.findElement(SENT_MAIL_SECTION_BUTTON_LOCATOR).click();
        waitForLoad(FIND_MAIL_WITHOUT_SUBJECT_LOCATOR);
        return webDriver.findElement(FIND_MAIL_WITHOUT_SUBJECT_LOCATOR).isDisplayed();
    }

    public boolean isMailInDraftExist(String subjectMail) {
        webDriver.findElement(DRAFTS_MAIL_SECTION_BUTTON_LOCATOR).click();
        waitForLoad(By.xpath(String.format(SUBJECT_MAIL, subjectMail)));
        return webDriver.findElement(By.xpath(String.format(SUBJECT_MAIL, subjectMail))).isDisplayed();
    }

    public boolean isMailInTrash(String subjectMail) {
        webDriver.findElement(TRASH_MAIL_SECTION_BUTTON_LOCATOR).click();
        waitForLoad(By.xpath(String.format(SUBJECT_MAIL, subjectMail)));
        return webDriver.findElement(By.xpath(String.format(SUBJECT_MAIL, subjectMail))).isDisplayed();
    }

    public void fastCleanTrash() {
        try {
            webDriver.findElement(CHOICE_FRAME_TRASH_CLEAN_LOCATOR).click();
            webDriver.findElement(CLEAN_TRASH_BUTTON_LOCATOR).click();
        } catch (NoSuchElementException e) {
            throw new RuntimeException("Trash is empty");
        }
    }

}
