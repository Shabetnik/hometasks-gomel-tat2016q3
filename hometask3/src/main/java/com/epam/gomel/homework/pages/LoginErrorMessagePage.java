package com.epam.gomel.homework.pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.PageFactory;

/**
 * Created by Uladzimir Shabetnik on 25.09.2016.
 */
public class LoginErrorMessagePage {
    private static final By LOGIN_ERROR_MESSAGE_LOCATOR = By.xpath("//div[@class='b-login__errors']");
    private WebDriver webDriver;

    public LoginErrorMessagePage(WebDriver webDriver) {
        this.webDriver = webDriver;
    }

    public String getIncorrectLoginMessage() {
        PageFactory.initElements(webDriver, LoginErrorMessagePage.class);
        return webDriver.findElement(LOGIN_ERROR_MESSAGE_LOCATOR).getText();
    }

}
