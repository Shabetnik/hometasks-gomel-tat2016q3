package com.epam.gomel.homework;

import com.epam.gomel.homework.pages.*;
import org.apache.commons.lang.RandomStringUtils;
import org.openqa.selenium.TimeoutException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import java.util.concurrent.TimeUnit;

/**
 * Created by Uladzimir Shabetnik on 25.09.2016.
 */
public class TestMailRu {
    private static final int IMPLICIT_WAIT_IN_SECONDS = 1;
    private static final String INCORRECT_LOGIN_MAIL = "ushabetnik";
    private static final String INCORRECT_PASSWORD_MAIL = "123";
    private static final String LOGIN_MAIL = "ushabetnik";
    private static final String PASSWORD_MAIL = "sail6270109";
    private static final String FULL_EMAIL = "ushabetnik@mail.ru";
    private static final String INCORRECT_LOGIN_MESSAGE
            = "Неверное имя пользователя или пароль. Проверьте правильность введенных данных.";
    private static final String SUCCESS_MAIL_SENT_MESSAGE = "Ваше письмо отправлено. Перейти во Входящие";
    private static final String ALERT_MAIL_WITHOUT_ADDRESS = "Не указан адрес получателя";
    private static final int PAGE_LOAD_TIME = 30;
    private static final int NUMBER_FOR_LENGTH = 5;
    private static final String SUBJECT_MAIL = "//a[@data-subject='%s']";
    private static final int TIME_FOR_WAITING_ARRIVE_LETTER = 2000;
    private WebDriver webDriver;
    private String subjectForMail;

    @BeforeClass
    public void setUpDriver() {
        //webDriver = new ChromeDriver();
        webDriver = new FirefoxDriver();
        webDriver.manage().timeouts().implicitlyWait(IMPLICIT_WAIT_IN_SECONDS, TimeUnit.SECONDS);
        webDriver.manage().timeouts().pageLoadTimeout(PAGE_LOAD_TIME, TimeUnit.SECONDS);
        webDriver.manage().window().maximize();
        webDriver.get("https://mail.ru/");
        subjectForMail = RandomStringUtils.random(NUMBER_FOR_LENGTH, new char[]{'a', 'b', 'c', 'd', 'e', 'f'});
    }

    @Test(description = "Must go in mail with correct login and password", dependsOnMethods = "loginErrorPageTest")
    public void loginCorrectTest() {
        webDriver.get("https://mail.ru/");
        LoginPage loginPage = new LoginPage(webDriver);
        loginPage.login(LOGIN_MAIL, PASSWORD_MAIL);
        LoginSuccessMessagePage loginSuccessMessagePage = new LoginSuccessMessagePage(webDriver);
        Assert.assertEquals(loginSuccessMessagePage.getCurrentUser(), FULL_EMAIL, "loginCorrectTest Error");
    }

    @Test(description = "Must go to page with error when login and password are incorrect")
    public void loginErrorPageTest() {
        LoginPage loginPage = new LoginPage(webDriver);
        LoginErrorMessagePage loginErrorMessagePage = loginPage.incorrectLogin(LOGIN_MAIL, INCORRECT_PASSWORD_MAIL);
        Assert.assertEquals(loginErrorMessagePage.getIncorrectLoginMessage(),
                INCORRECT_LOGIN_MESSAGE, "loginErrorPageTest error");
    }

    @Test(description = "Must get alert message when in letter empty address", dependsOnMethods = "loginCorrectTest")
    public void sentMailWithoutAddressTest() {
        TopMenuPage topMenuPage = new TopMenuPage(webDriver);
        String alertText = topMenuPage.sentMailWithoutAddress();
        Assert.assertEquals(alertText, ALERT_MAIL_WITHOUT_ADDRESS, "sentMailWithoutAddressTest error");
    }

    @Test(description = "Must send a letter with body and subject and address "
            + "and go to page with success sent mail message", dependsOnMethods = "sentMailWithoutAddressTest")
    public void sentMailWithAddressAndSubjectAndTextTest() throws InterruptedException {
        TopMenuPage sentMailPage = new TopMenuPage(webDriver);
        SentSuccessMessagePage sentSuccessMessagePage = sentMailPage.sentMailWithAllFields("ushabetnik@mail.ru",
                subjectForMail, "TEXT");
        Assert.assertTrue(sentSuccessMessagePage.isMessageSent(), "sentMailWithAddressAndSubjectAndTextTest error");
        Thread.sleep(TIME_FOR_WAITING_ARRIVE_LETTER);
    }

    @Test(description = "Must check a letter with some unique subject in 'inbox' section",
            dependsOnMethods = "sentMailWithAddressAndSubjectAndTextTest")
    public void isSentMailWithSubjectInInboxSectionTest() {
        LeftMenuPage leftMenuPage = new LeftMenuPage(webDriver);
        Assert.assertTrue(leftMenuPage.isInboxMailWithSubjectExist(subjectForMail),
                "isSentMailWithSubjectInInboxSectionTest error");
    }

    @Test(description = "Must check a letter with some uniq subject in 'sent' section",
            dependsOnMethods = "isSentMailWithSubjectInInboxSectionTest")
    public void isSentMailWithSubjectInSentSectionTest() {
        LeftMenuPage leftMenuPage = new LeftMenuPage(webDriver);
        leftMenuPage.isSentMailWithSubjectExist(subjectForMail);
        Assert.assertTrue(leftMenuPage.isSentMailWithSubjectExist(subjectForMail),
                "isSentMailWithSubjectInSentSectionTest error");
    }

    @Test(description = "Must send a letter with address and without subject and text",
            dependsOnMethods = "isSentMailWithSubjectInSentSectionTest")
    public void sendMailWithAddressWithoutSubjectAndTextTest() throws InterruptedException {
        TopMenuPage topMenuPage = new TopMenuPage(webDriver);
        SentSuccessMessagePage sentSuccessMessagePage = topMenuPage.sentMailWithoutSubject("ushabetnik@mail.ru");
        Assert.assertTrue(sentSuccessMessagePage.isMessageSent(), "sendMailWithAddressWithoutSubjectAndTextTest error");
        Thread.sleep(TIME_FOR_WAITING_ARRIVE_LETTER);
    }

    @Test(description = "Must check a letter without subject in 'inbox' section",
            dependsOnMethods = "sendMailWithAddressWithoutSubjectAndTextTest")
    public void isSentMailWithoutSubjectInInboxSectionTest() {
        LeftMenuPage leftMenuPage = new LeftMenuPage(webDriver);
        Assert.assertTrue(leftMenuPage.isInboxMailWithoutSubjectExist(),
                "isSentMailWithoutSubjectInInboxSectionTest error");
    }

    @Test(description = "Must check a letter without subject in 'sent' section",
            dependsOnMethods = "isSentMailWithoutSubjectInInboxSectionTest")
    public void isSentMailWithoutSubjectInSentSectionTest() throws InterruptedException {
        LeftMenuPage leftMenuPage = new LeftMenuPage(webDriver);
        Assert.assertTrue(leftMenuPage.isSentMailWithoutSubjectExist(),
                "isSentMailWithoutSubjectInSentSectionTest error");
    }

    @Test(description = "Must save a letter to 'Draft' section",
            dependsOnMethods = "isSentMailWithoutSubjectInSentSectionTest")
    public void saveMailInDraftSectionTest() {
        TopMenuPage topMenuPage = new TopMenuPage(webDriver);
        SuccessSaveInDraftPage successSaveInDraftPage = topMenuPage.saveMailInDrafts("ushabetnik@mail.ru",
                subjectForMail, "TEXT");
        Assert.assertTrue(successSaveInDraftPage.isMailSaveInDraft(), "saveMailInDraftSectionTest error");
    }

    @Test(description = "Must check a letter with subject in 'drafts' section",
            dependsOnMethods = "saveMailInDraftSectionTest")
    public void isMailInDraftsSectionTest() {
        LeftMenuPage leftMenuPage = new LeftMenuPage(webDriver);
        Assert.assertTrue(leftMenuPage.isMailInDraftExist(subjectForMail), "isMailWithInDraftsSectionTest error");
    }

    @Test(description = "Must delete mail with subject from draft section",
            dependsOnMethods = "isMailInDraftsSectionTest")
    public void deleteMailInDraftsSectionWithSubjectTest() {
        TopMenuPage topMenuPage = new TopMenuPage(webDriver);
        Assert.assertTrue(topMenuPage.deleteMailInDrafts(subjectForMail));
    }

    @Test(description = "Must check mail in 'trash' section",
            dependsOnMethods = "deleteMailInDraftsSectionWithSubjectTest")
    public void isMailInTrashSectionTest() {
        LeftMenuPage leftMenuPage = new LeftMenuPage(webDriver);
        TopMenuPage topMenuPage = new TopMenuPage(webDriver);
        topMenuPage.deleteMailInDrafts(subjectForMail);
        Assert.assertTrue(leftMenuPage.isMailInTrash(subjectForMail), "deletePermanentlyMailFromTrash error");
    }

    @Test(description = "Must permanently delete a letter with subject from 'trash' section",
            dependsOnMethods = "isMailInTrashSectionTest", expectedExceptions = TimeoutException.class)
    public void deletePermanentlyMailFromTrash() {
        LeftMenuPage leftMenuPage = new LeftMenuPage(webDriver);
        leftMenuPage.fastCleanTrash();
        Assert.assertTrue(leftMenuPage.isMailInTrash(subjectForMail), "deletePermanentlyMailFromTrash error");
    }

    @AfterClass
    public void driverQuit() {
        webDriver.quit();
    }
}
