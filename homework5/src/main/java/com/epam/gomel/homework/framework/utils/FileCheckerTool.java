package com.epam.gomel.homework.framework.utils;

import org.apache.commons.io.FileUtils;

import java.io.File;
import java.io.IOException;

/**
 * Created by Uladzimir Shabetnik on 09.10.2016.
 */
public class FileCheckerTool {
    private static final String PATH_DOWNLOADED_FILE = "./src/main/resources/downloads/%s.txt";
    private static final String PATH_DOWNLOADED_DIRECTORY = "./src/main/resources/downloads";
    private static final String PATH_ACTUAL_FILE = "./src/main/resources/generated/%s.txt";
    private static final String PATH_GENERATED_DIRECTORY = "./src/main/resources/generated/";
    private static final int WAIT_FILE_DOWNLOAD_SECONDS = 30;
    private File fileDownloadContent;
    private File fileExistContent;

    public void waitForFile(String fileName) {
        fileDownloadContent = new File(String.format(PATH_DOWNLOADED_FILE, fileName));
        FileUtils.waitFor(fileDownloadContent, WAIT_FILE_DOWNLOAD_SECONDS);
    }

    public boolean isContentFileSame(String fileName) throws IOException {
        fileExistContent = new File(new File(String.format(PATH_ACTUAL_FILE, fileName)).getAbsolutePath());
        return FileUtils.contentEquals(fileDownloadContent, fileExistContent);
    }

    public void deleteDownloadDirectory() throws IOException {
        FileUtils.deleteDirectory(new File(PATH_DOWNLOADED_DIRECTORY));
    }

    public void deleteGenerateFilesDirectory() throws IOException {
        FileUtils.deleteDirectory(new File(PATH_GENERATED_DIRECTORY));
    }
}
