package com.epam.gomel.homework.test.yandexdisk;

import com.epam.gomel.homework.product.yandexdisk.account.bo.YaDiskAccountFactory;
import com.epam.gomel.homework.product.yandexdisk.account.service.YaDiskLoginService;
import org.testng.annotations.Test;

/**
 * Created by Uladzimir Shabetnik on 08.10.2016.
 */
public class YaDiskLoginTest {
    private YaDiskLoginService yaDiskLoginService = new YaDiskLoginService();

    @Test(description = "Login to 'disk.yandex.ru' from login page")
    public void loginToYaDiskFromLoginPage() {
        yaDiskLoginService.login(YaDiskAccountFactory.createCorrectDataAccount());
    }

    @Test(description = "Check failed authentication error message")
    public void checkFailedLoginErrorMessage() {
        yaDiskLoginService.unsafeLogin(YaDiskAccountFactory.createWrongDataAccount());
        boolean expectedResult = true;
        yaDiskLoginService.checkFailedLogin(expectedResult);
    }

}
