package com.epam.gomel.homework.product.mailru.common.exception;

import com.epam.gomel.homework.framework.runner.CommonTestRuntimeException;

/**
 * Created by Uladzimir Shabetnik on 09.10.2016.
 */
public class MailRuDeleteException extends CommonTestRuntimeException {

    public MailRuDeleteException(String message) {
        super(message);
    }
}
