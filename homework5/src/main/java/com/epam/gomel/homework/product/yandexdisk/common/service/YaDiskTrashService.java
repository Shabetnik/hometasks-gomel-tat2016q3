package com.epam.gomel.homework.product.yandexdisk.common.service;

import com.epam.gomel.homework.product.yandexdisk.common.exception.YaDiskFailedOperationException;
import com.epam.gomel.homework.product.yandexdisk.common.page.YaDiskContentPage;
import com.epam.gomel.homework.product.yandexdisk.common.page.YaDiskRightMenuPage;
import org.openqa.selenium.NoSuchElementException;
import java.util.Map;

/**
 * Created by Uladzimir Shabetnik on 09.10.2016.
 */
public class YaDiskTrashService {
    private YaDiskContentPage yaDiskContentPage = new YaDiskContentPage();
    private YaDiskRightMenuPage yaDiskRightMenuPage = new YaDiskRightMenuPage();

    public void moveFileToTrash(String fileName) {
        yaDiskContentPage
                .findFile(fileName)
                .choiceFile()
                .dragFileToTrash();
    }

    public void moveSeveralFilesToTrash(Map<String, String> filesList) {
        yaDiskContentPage.dragTwoFileToTrash(filesList);
    }

    public void checkFileInTrash(String fileName) {
        yaDiskContentPage.enterToTrash();
        try {
            yaDiskContentPage.findFile(fileName);
        } catch (NoSuchElementException e) {
            throw new YaDiskFailedOperationException("File was not remove to trash section");
        }
    }

    public void checkSeveralFilesInTrash(Map<String, String> filesList) {
        yaDiskContentPage.enterToTrash();
        try {
            yaDiskContentPage.findSeveralFiles(filesList);
        } catch (NoSuchElementException e) {
            throw new YaDiskFailedOperationException("File was not remove to trash section");
        }
    }

    public void deleteFileFromTrash(String file) {
        yaDiskContentPage
                .enterToTrash()
                .findFile(file)
                .choiceFile();
        yaDiskRightMenuPage.deleteFile();
    }

    public void checkFileWasDeleted(String fileName) {
        yaDiskContentPage.enterToTrash();
        try {
            yaDiskContentPage.waitForFileNotInSection(fileName);
        } catch (NoSuchElementException e) {
            throw new YaDiskFailedOperationException("File was not deleted");
        }
    }

}
