package com.epam.gomel.homework.framework.ui.element;

import com.epam.gomel.homework.framework.ui.browser.Browser;
import org.openqa.selenium.Keys;
import org.openqa.selenium.interactions.Actions;

/**
 * Created by Uladzimir Shabetnik on 09.10.2016.
 */
public class ActionWrapper {
    private Actions actions = Browser.getBrowser().getActions();

    public Actions getActionOnElement() {
        return actions;
    }

    public ActionWrapper clickAndHold(Element element) {
        getActionOnElement().clickAndHold(element.getWrappedWebElement());
        return this;
    }

    public ActionWrapper moveToElement(Element element) {
        getActionOnElement().moveToElement(element.getWrappedWebElement());
        return this;
    }

    public ActionWrapper doubleClick(Element element) {
        getActionOnElement().doubleClick(element.getWrappedWebElement());
        return this;
    }

    public ActionWrapper release() {
        getActionOnElement().release();
        return this;
    }

    public ActionWrapper buildAndPerform() {
        getActionOnElement().build().perform();
        return this;
    }

    public ActionWrapper perform() {
        getActionOnElement().perform();
        return this;
    }

    public ActionWrapper click(Element element) {
        getActionOnElement().click(element.getWrappedWebElement());
        return this;
    }

    public ActionWrapper keyDown(Keys keys) {
        getActionOnElement().keyDown(keys);
        return this;
    }

    public ActionWrapper keyUp(Keys keys) {
        getActionOnElement().keyUp(keys);
        return this;
    }
}
