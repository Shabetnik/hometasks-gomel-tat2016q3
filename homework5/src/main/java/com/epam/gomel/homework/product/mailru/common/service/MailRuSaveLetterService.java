package com.epam.gomel.homework.product.mailru.common.service;

import com.epam.gomel.homework.product.mailru.common.bo.MailRuLetter;
import com.epam.gomel.homework.product.mailru.common.page.MailRuContentPage;
import com.epam.gomel.homework.product.mailru.common.page.MailRuTopMenuPage;

/**
 * Created by Uladzimir Shabetnik on 08.10.2016.
 */
public class MailRuSaveLetterService {

    public boolean isMailSavedInDrafts(MailRuLetter mailRuLetter) {
        MailRuTopMenuPage mailRuTopMenuPage = new MailRuTopMenuPage();
        MailRuContentPage mailRuContentPage = new MailRuContentPage();
        mailRuTopMenuPage.openToWriteMail();
        mailRuContentPage
                .setMailAddress(mailRuLetter.getAddress())
                .setMailSubject(mailRuLetter.getSubject())
                .setMailBodyText(mailRuLetter.getText());
        mailRuTopMenuPage.saveInDraftMail();
        return mailRuContentPage.isMailSuccessSaveInDraft();
    }
}
