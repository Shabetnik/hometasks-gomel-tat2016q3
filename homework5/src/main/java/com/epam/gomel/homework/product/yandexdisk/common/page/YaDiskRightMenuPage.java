package com.epam.gomel.homework.product.yandexdisk.common.page;

import com.epam.gomel.homework.framework.ui.element.Element;
import org.openqa.selenium.By;

/**
 * Created by Uladzimir Shabetnik on 08.10.2016.
 */
public class YaDiskRightMenuPage {

    private static final By BUTTON_DOWNLOAD_LOCATOR = By.xpath("//button[@data-click-action='resource.download']");
    private static final By BUTTON_PERMANENTLY_DELETE_LOCATOR =
            By.xpath("//button[@data-click-action='resource.delete']");
    private static final By BUTTON_RESTORE_LOCATOR = By.xpath("//button[@data-click-action='resource.restore']");

    private Element downloadButton = new Element(BUTTON_DOWNLOAD_LOCATOR);
    private Element deleteButton = new Element(BUTTON_PERMANENTLY_DELETE_LOCATOR);
    private Element restoreButton = new Element(BUTTON_RESTORE_LOCATOR);

    public void downloadFile() {
        downloadButton.waitForVisible();
        downloadButton.click();
    }

    public void deleteFile() {
        deleteButton.waitForVisible();
        deleteButton.click();
    }

    public void restoreFile() {
        restoreButton.waitForVisible();
        restoreButton.click();
    }

}


