package com.epam.gomel.homework.test.yandexdisk;

import com.epam.gomel.homework.framework.utils.FileGenerator;
import com.epam.gomel.homework.product.yandexdisk.account.bo.YaDiskAccountFactory;
import com.epam.gomel.homework.product.yandexdisk.account.service.YaDiskLoginService;
import com.epam.gomel.homework.product.yandexdisk.common.service.YaDiskTrashService;
import com.epam.gomel.homework.product.yandexdisk.common.service.YaDiskUploadFileService;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by Uladzimir Shabetnik on 09.10.2016.
 */
public class YaDiskTrashSeveralFilesTest {
    private static final int COUNT_OF_FILES = 5;
    private YaDiskLoginService yaDiskLoginService = new YaDiskLoginService();
    private YaDiskTrashService yaDiskTrashService = new YaDiskTrashService();
    private YaDiskUploadFileService yaDiskUploadFileService = new YaDiskUploadFileService();
    private FileGenerator fileGenerator = new FileGenerator();
    private Map<String, String> filesList = new HashMap<>();

    @BeforeClass
    public void setUpClass() throws IOException {
        filesList = fileGenerator.generateSeveralFiles(COUNT_OF_FILES);
    }

    @BeforeMethod
    public void setUp() {
        yaDiskLoginService.login(YaDiskAccountFactory.createCorrectDataAccount());
    }

    @Test(description = "Upload two files and check success")
    public void uploadSeveralFiles() throws IOException {
        for (Map.Entry<String, String> entry : filesList.entrySet()) {
            yaDiskUploadFileService.uploadFile(entry.getKey(), entry.getValue());
            yaDiskUploadFileService.checkFileUploaded(entry.getKey());
        }
        yaDiskUploadFileService.cleanGeneratedFiles();
    }

    @Test(description = "Remove several files to trash section", dependsOnMethods = "uploadSeveralFiles")
    public void removeFilesToTrashTest() throws IOException {
        yaDiskTrashService.moveSeveralFilesToTrash(filesList);
        yaDiskTrashService.checkSeveralFilesInTrash(filesList);
    }
}

