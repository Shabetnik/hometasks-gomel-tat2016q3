package com.epam.gomel.homework.product.yandexdisk.common.exception;

import com.epam.gomel.homework.framework.runner.CommonTestRuntimeException;

/**
 * Created by Uladzimir Shabetnik on 09.10.2016.
 */
public class YaDiskFailedOperationException extends CommonTestRuntimeException {
    public YaDiskFailedOperationException(String message) {
        super(message);
    }
}
