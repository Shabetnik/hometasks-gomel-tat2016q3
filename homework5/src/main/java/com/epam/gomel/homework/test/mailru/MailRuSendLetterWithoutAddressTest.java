package com.epam.gomel.homework.test.mailru;

import com.epam.gomel.homework.product.mailru.account.bo.MailRuAccountFactory;
import com.epam.gomel.homework.product.mailru.common.service.MailRuSendLetterService;
import org.testng.Assert;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;
import com.epam.gomel.homework.product.mailru.account.service.MailRuLoginService;

/**
 * Created by Uladzimir Shabetnik on 09.10.2016.
 */
public class MailRuSendLetterWithoutAddressTest {
    private static final String EXPECTED_SUCCESS_SENT_MAIL_MESSAGE = "Не указан адрес получателя";
    private MailRuLoginService mailRuLoginService = new MailRuLoginService();
    private MailRuSendLetterService mailRuSendLetterService = new MailRuSendLetterService();

    @BeforeMethod
    public void setUp() {
        mailRuLoginService.login(MailRuAccountFactory.createCorrectDataAccount());
    }

    @Test(description = "Must send a letter without address and get alert")
    public void sentMailWithoutAddressTest() throws InterruptedException {
        String actualResult = mailRuSendLetterService.sendMailWithoutAddress();
        Assert.assertEquals(actualResult, EXPECTED_SUCCESS_SENT_MAIL_MESSAGE, "sentMailWithoutAddressTest error");
    }

}
