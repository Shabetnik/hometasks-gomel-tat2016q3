package com.epam.gomel.homework.product.yandexdisk.account.page;

import com.epam.gomel.homework.framework.ui.browser.Browser;
import com.epam.gomel.homework.framework.ui.element.Element;
import com.epam.gomel.homework.product.yandexdisk.common.YaDiskGlobalParameters;
import org.openqa.selenium.By;

/**
 * Created by Uladzimir Shabetnik on 08.10.2016.
 */
public class YaDiskLoginPage {
    private static final String URL = YaDiskGlobalParameters.DEFAULT_PRODUCT_URL;
    private static final By LOGIN_INPUT_LOCATOR = By.xpath("//input[@name='login']");
    private static final By PASSWORD_INPUT_LOCATOR = By.xpath("//input[@name='password']");
    private static final By BUTTON_LOGIN_LOCATOR = By.xpath("//div[@class='nb-input-group']//button");
    private static final By LOGIN_FAILED_LOCATOR = By.xpath("//label["
            + "@class='nb-input _nb-complex-input nb-form-auth__input _init _nb-is-wrong']");

    private Element loginInput = new Element(LOGIN_INPUT_LOCATOR);
    private Element passwordInput = new Element(PASSWORD_INPUT_LOCATOR);
    private Element buttonLogin = new Element(BUTTON_LOGIN_LOCATOR);
    private Element errorLogin = new Element(LOGIN_FAILED_LOCATOR);

    public YaDiskLoginPage open() {
        Browser.getBrowser().open(URL);
        return this;
    }

    public YaDiskLoginPage setLogin(String login) {
        loginInput.typeValue(login);
        return this;
    }

    public YaDiskLoginPage setPassword(String password) {
        passwordInput.typeValue(password);
        return this;
    }

    public void signIn() {
        buttonLogin.click();
    }

    public boolean checkFailedLogin() {
        errorLogin.waitForVisible();
        return errorLogin.isVisible();
    }

}
