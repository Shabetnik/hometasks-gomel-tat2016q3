package com.epam.gomel.homework.product.mailru.common.bo;

import lombok.Getter;
import lombok.Setter;

/**
 * Created by Uladzimir Shabetnik on 08.10.2016.
 */
public class MailRuLetter {
    @Getter @Setter
    private String address;
    @Getter @Setter
    private String subject;
    @Getter @Setter
    private String text;


}
