package com.epam.gomel.homework.product.yandexdisk.common.page;

import com.epam.gomel.homework.framework.ui.element.Element;
import org.openqa.selenium.By;

/**
 * Created by Uladzimir Shabetnik on 08.10.2016.
 */
public class YaDiskLeftMenuPage {
    private static final By FILE_SECTION_LOCATOR = By.xpath("//a[@href='/client/disk']");
    private static final By FILE_SECTION_ACTIVE_LOCATOR
            = By.xpath("//a[@class='navigation__item navigation__item_current' and @href='/client/disk']");

    private Element filesLink = new Element(FILE_SECTION_LOCATOR);
    private Element filesIsActive = new Element(FILE_SECTION_ACTIVE_LOCATOR);

    public void enterToFilesSection() {
        filesLink.waitForVisible();
        filesLink.click();
        filesIsActive.waitForVisible();
    }

}
