package com.epam.gomel.homework.product.mailru.account.page;

import com.epam.gomel.homework.framework.ui.element.Element;
import com.epam.gomel.homework.framework.ui.browser.Browser;
import org.openqa.selenium.By;
import com.epam.gomel.homework.product.mailru.common.MailRuGlobalParameters;

/**
 * Created by Uladzimir Shabetnik on 08.10.2016.
 */
public class MailRuLoginPage {
    private static final String URL = MailRuGlobalParameters.DEFAULT_PRODUCT_URL;
    private static final By LOGIN_INPUT_LOCATOR = By.xpath("//input[@id='mailbox__login']");
    private static final By PASSWORD_INPUT_LOCATOR = By.xpath("//input[@id='mailbox__password']");
    private static final By BUTTON_LOGIN_LOCATOR = By.xpath("//input[@id='mailbox__auth__button']");
    private static final By LOGIN_ERROR_MESSAGE_LOCATOR = By.xpath("//div[@class='b-login__errors']");

    private Element loginInput = new Element(LOGIN_INPUT_LOCATOR);
    private Element passwordInput = new Element(PASSWORD_INPUT_LOCATOR);
    private Element buttonLogin = new Element(BUTTON_LOGIN_LOCATOR);
    private Element errorMessage = new Element(LOGIN_ERROR_MESSAGE_LOCATOR);

    public MailRuLoginPage open() {
        Browser.getBrowser().open(URL);
        return this;
    }

    public MailRuLoginPage setLogin(String login) {
        loginInput.typeValue(login);
        return this;
    }

    public MailRuLoginPage setPassword(String password) {
        passwordInput.typeValue(password);
        return this;
    }

    public void signIn() {
        buttonLogin.click();
    }

    public String getErrorMessage() {
        return errorMessage.getText();
    }

}
