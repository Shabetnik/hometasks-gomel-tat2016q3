package com.epam.gomel.homework.test.mailru;

import com.epam.gomel.homework.product.mailru.account.bo.MailRuAccountFactory;
import com.epam.gomel.homework.product.mailru.account.service.MailRuLoginService;
import org.testng.annotations.Test;

/**
 * Created by Uladzimir Shabetnik on 08.10.2016.
 */
public class MailRuLoginTest {
    private static final String EXPECTED_FAILED_LOGIN_MESSAGE
            = "Неверное имя пользователя или пароль. Проверьте правильность введенных данных.";
    private MailRuLoginService mailRuLoginService = new MailRuLoginService();

    @Test(description = "Login to 'mail.ru' from login page")
    public void loginToMailRuFromLoginPage() {
        mailRuLoginService.login(MailRuAccountFactory.createCorrectDataAccount());
    }

    @Test(description = "Check failed authentication error message")
    public void checkFailedLoginErrorMessage() {
        mailRuLoginService.unsafeLogin(MailRuAccountFactory.createWrongDataAccount());
        mailRuLoginService.checkErrorMessage(EXPECTED_FAILED_LOGIN_MESSAGE);
    }

}
