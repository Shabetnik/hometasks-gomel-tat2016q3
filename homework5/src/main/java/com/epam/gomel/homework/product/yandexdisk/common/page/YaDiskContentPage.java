package com.epam.gomel.homework.product.yandexdisk.common.page;

import com.epam.gomel.homework.framework.ui.element.ActionWrapper;
import com.epam.gomel.homework.framework.ui.element.Element;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.NoSuchElementException;
import java.util.Map;

/**
 * Created by Uladzimir Shabetnik on 09.10.2016.
 */
public class YaDiskContentPage {
    private static final String FILE_NAME_LOCATOR = "//div[contains(@title,'%s')]";
    private static final By TRASH_LOCATOR = By.xpath("//div[@title='Корзина']");
    private static final By IN_TRASH_SECTION_CHECK_LOCATOR = By.xpath("//span[@class='crumbs__current']");
    private Element file;
    private Element trash = new Element(TRASH_LOCATOR);
    private Element checkInTrashSection = new Element(IN_TRASH_SECTION_CHECK_LOCATOR);

    public YaDiskContentPage findFile(String fileName) throws NoSuchElementException {
        file = new Element(By.xpath(String.format(FILE_NAME_LOCATOR, fileName)));
        file.waitForVisible();
        return this;
    }

    public YaDiskContentPage waitForFileNotInSection(String fileName) throws NoSuchElementException {
        file = new Element(By.xpath(String.format(FILE_NAME_LOCATOR, fileName)));
        file.waitForDisappear();
        return this;
    }

    public YaDiskContentPage choiceFile() {
        file.waitForAppear();
        file.click();
        return this;
    }

    public YaDiskContentPage dragFileToTrash() {
        ActionWrapper actions = new ActionWrapper();
        trash.waitForVisible();
        actions.clickAndHold(file)
                .moveToElement(trash)
                .release()
                .buildAndPerform();
        return this;
    }

    public YaDiskContentPage dragTwoFileToTrash(Map<String, String> filesList) {
        ActionWrapper actions = new ActionWrapper();
        trash.waitForVisible();
        for (Map.Entry<String, String> entry : filesList.entrySet()) {
            file = new Element(By.xpath(String.format(FILE_NAME_LOCATOR, entry.getKey())));
            file.waitForVisible();
            actions
                    .keyDown(Keys.CONTROL)
                    .click(file)
                    .buildAndPerform();
        }
        actions
                .clickAndHold(file)
                .moveToElement(trash)
                .release()
                .buildAndPerform();
        return this;
    }

    public YaDiskContentPage findSeveralFiles(Map<String, String> filesList) throws NoSuchElementException {
        for (Map.Entry<String, String> entry : filesList.entrySet()) {
            file = new Element(By.xpath(String.format(FILE_NAME_LOCATOR, entry.getKey())));
            file.waitForVisible();
        }
        return this;
    }

    public YaDiskContentPage enterToTrash() {
        ActionWrapper actions = new ActionWrapper();
        trash.waitForVisible();
        actions.doubleClick(trash).buildAndPerform();
        checkInTrashSection.waitForVisible();
        return this;
    }

}
