package com.epam.gomel.homework.product.mailru.common.service;

import com.epam.gomel.homework.product.mailru.common.bo.MailRuLetter;
import com.epam.gomel.homework.product.mailru.common.exception.MailRuSendMailException;
import com.epam.gomel.homework.framework.ui.element.AlertWrapper;
import com.epam.gomel.homework.product.mailru.common.page.MailRuContentPage;
import com.epam.gomel.homework.product.mailru.common.page.MailRuTopMenuPage;

/**
 * Created by Uladzimir Shabetnik on 08.10.2016.
 */
public class MailRuSendLetterService {
    MailRuTopMenuPage mailRuTopMenuPage = new MailRuTopMenuPage();
    MailRuContentPage mailRuContentPage = new MailRuContentPage();

    public void sendMailWithFields(MailRuLetter mailRuLetter) {
        mailRuTopMenuPage.openToWriteMail();
        mailRuContentPage
                .setMailAddress(mailRuLetter.getAddress())
                .setMailSubject(mailRuLetter.getSubject())
                .setMailBodyText(mailRuLetter.getText())
                .attachFileToMail();
        mailRuTopMenuPage.sendMail();
    }

    public void checkSuccessSentMailMessage(String expectedMessage) {
        String actualMessage = new MailRuContentPage().getSuccessSentMessage();
        if (!actualMessage.equals(expectedMessage)) {
            throw new MailRuSendMailException(String.format(
                    "Expected message is '%s' but actual is '%s'", expectedMessage, actualMessage));
        }
    }

    public String sendMailWithoutAddress() {
        mailRuTopMenuPage.openToWriteMail();
        mailRuTopMenuPage.sendMail();
        AlertWrapper alertWrapper = new AlertWrapper();
        String message = alertWrapper.getAlertText();
        alertWrapper.acceptAlert();
        return message;
    }

    public void sendMailWithoutSubject(MailRuLetter mailRuLetter) {
        mailRuTopMenuPage.openToWriteMail();
        mailRuContentPage.setMailAddress(mailRuLetter.getAddress()).setMailBodyText(mailRuLetter.getText());
        mailRuTopMenuPage.sendMail();
    }

}
