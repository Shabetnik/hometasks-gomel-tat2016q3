package com.epam.gomel.homework.product.yandexdisk.account.exception;

import com.epam.gomel.homework.framework.runner.CommonTestRuntimeException;

/**
 * Created by Uladzimir Shabetnik on 08.10.2016.
 */
public class YaDiskLoginException extends CommonTestRuntimeException {

    public YaDiskLoginException(String message) {
        super(message);
    }
}
