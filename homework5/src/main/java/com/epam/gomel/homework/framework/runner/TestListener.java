package com.epam.gomel.homework.framework.runner;

import com.epam.gomel.homework.framework.ui.browser.Browser;
import org.testng.ITestResult;
import org.testng.TestListenerAdapter;

/**
 * Created by Uladzimir Shabetnik on 08.10.2016.
 */
public class TestListener extends TestListenerAdapter {
    @Override
    public void onTestSuccess(ITestResult tr) {
        Browser.getBrowser().close();
    }

    @Override
    public void onTestFailure(ITestResult tr) {
        Browser.getBrowser().close();
    }

}
