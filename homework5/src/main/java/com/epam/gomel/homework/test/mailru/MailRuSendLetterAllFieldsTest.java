package com.epam.gomel.homework.test.mailru;

import org.testng.Assert;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;
import com.epam.gomel.homework.product.mailru.account.bo.MailRuAccountFactory;
import com.epam.gomel.homework.product.mailru.account.service.MailRuLoginService;
import com.epam.gomel.homework.product.mailru.common.bo.MailRuLetter;
import com.epam.gomel.homework.product.mailru.common.bo.MailRuLetterFactory;
import com.epam.gomel.homework.product.mailru.common.service.MailRuFindLetterService;
import com.epam.gomel.homework.product.mailru.common.service.MailRuSendLetterService;

/**
 * Created by Uladzimir Shabetnik on 08.10.2016.
 */
public class MailRuSendLetterAllFieldsTest {
    private static final String EXPECTED_SUCCESS_SENT_MAIL_MESSAGE = "Ваше письмо отправлено. Перейти во Входящие";
    private MailRuLoginService mailRuLoginService = new MailRuLoginService();
    private MailRuSendLetterService mailRuSendLetterService = new MailRuSendLetterService();
    private MailRuFindLetterService mailRuFindLetterService = new MailRuFindLetterService();
    private String mailSubject;

    @BeforeMethod
    public void setUp() {
        mailRuLoginService.login(MailRuAccountFactory.createCorrectDataAccount());
    }

    @Test(description = "Must send a letter with body and subject and address "
            + "and go to page with success sent mail message")
    public void sentMailWithAddressAndSubjectAndTextTest() throws InterruptedException {
        MailRuLetter mailRuLetter = MailRuLetterFactory.createMailWithAllFields();
        mailRuSendLetterService.sendMailWithFields(mailRuLetter);
        mailSubject = mailRuLetter.getSubject();
        mailRuSendLetterService.checkSuccessSentMailMessage(EXPECTED_SUCCESS_SENT_MAIL_MESSAGE);
    }

    @Test(description = "Must check a letter with some unique subject in 'inbox' section",
            dependsOnMethods = "sentMailWithAddressAndSubjectAndTextTest")
    public void isSentMailWithSubjectInInboxSectionTest() {
        Boolean actualResult = mailRuFindLetterService.isMailFoundInInboxBySubject(mailSubject);
        Assert.assertTrue(actualResult, "isSentMailWithSubjectInInboxSectionTest error");
    }

    @Test(description = "Must check a letter with some uniq subject in 'sent' section",
            dependsOnMethods = "isSentMailWithSubjectInInboxSectionTest")
    public void isSentMailWithSubjectInSentSectionTest() {
        Boolean actualResult = mailRuFindLetterService.isMailFoundInSentBySubject(mailSubject);
        Assert.assertTrue(actualResult, "isSentMailWithSubjectInSentSectionTest error");
    }

}
