package com.epam.gomel.homework.product.mailru.common.service;

import com.epam.gomel.homework.product.mailru.common.exception.MailRuDeleteException;
import com.epam.gomel.homework.product.mailru.common.page.MailRuContentPage;
import com.epam.gomel.homework.product.mailru.common.page.MailRuLeftMenuPage;
import org.openqa.selenium.TimeoutException;
import com.epam.gomel.homework.product.mailru.common.page.MailRuTopMenuPage;

/**
 * Created by Uladzimir Shabetnik on 08.10.2016.
 */
public class MailRuDeleteLetterService {
    MailRuContentPage mailRuContentPage = new MailRuContentPage();
    MailRuLeftMenuPage mailRuLeftMenuPage = new MailRuLeftMenuPage();
    MailRuTopMenuPage mailRuTopMenuPage = new MailRuTopMenuPage();

    public void deleteNeededMailFromDrafts(String subject) {
        mailRuLeftMenuPage.enterToDraftsMail();
        mailRuContentPage.clickNeededMailCheckbox(subject);
        mailRuTopMenuPage.deleteSelectedMail();
    }

    public void deleteAllMailInTrash() {
        mailRuLeftMenuPage.cleanAllMailInTrash();
    }

    public void checkMailDeletedFromTrash(String subject) {
        mailRuLeftMenuPage.enterToTrashMail();
        try {
            mailRuContentPage.waitDisappearNeededMailBySubject(subject);
        } catch (TimeoutException e) {
            throw new MailRuDeleteException("MailRuLetter delete failed");
        }
    }

    public void checkMailDeletedFromDrafts(String subject) {
        mailRuLeftMenuPage.enterToDraftsMail();
        try {
            mailRuContentPage.waitDisappearNeededMailBySubject(subject);
        } catch (TimeoutException e) {
            throw new MailRuDeleteException("MailRuLetter delete failed");
        }
    }
}
