package com.epam.gomel.homework.framework.runner.mailru;

import org.testng.TestNG;
import java.util.Arrays;
import java.util.List;

/**
 * Created by Uladzimir Shabetnik on 08.10.2016.
 */
public class RunnerMailRu {
    public static void main(String[] args) {
        TestNG testNG = new TestNG();
        List<String> files = Arrays.asList("test-suites/mailrutestsuite.xml");
        testNG.setTestSuites(files);
        testNG.run();
    }
}
