package com.epam.gomel.homework;

import lombok.Getter;
import lombok.Setter;

public class Boy extends AbstractHuman {
    private static final double GOOD_WEALTH = 1_000_000;
    @Setter
    private Month birthdayMonth;
    @Setter
    private Girl girlFriend;
    @Getter
    @Setter
    private double wealth;

    public Boy(Month birthdayMonth, double wealth, Girl girlFriend) {
        this.birthdayMonth = birthdayMonth;
        this.wealth = wealth;
        this.girlFriend = girlFriend;
        if (girlFriend != null) {
            this.girlFriend.setBoyFriend(this);
        }
    }

    public Boy() {
        super();
    }

    public Boy(Month birthdayMonth) {
        this(birthdayMonth, 0, null);
    }

    public Boy(double wealth) {
        this(null, wealth, null);
    }

    public Boy(Girl girl) {
        this(null, 0, girl);
    }

    @Override
    public Mood getMood() {
        if (isRich() && isPrettyGirlFriend() && isSummerMonth()) {
            return Mood.EXCELLENT;
        } else if (isRich() && isPrettyGirlFriend()) {
            return Mood.GOOD;
        } else if (isRich() && isSummerMonth()) {
            return Mood.NEUTRAL;
        } else if (isRich() || isPrettyGirlFriend() || isSummerMonth()) {
            return Mood.BAD;
        }
        return Mood.HORRIBLE;
    }

    public void spendSomeMoney(double amountForSpending) {
        if (amountForSpending <= wealth) {
            wealth -= amountForSpending;
        } else {
            throw new RuntimeException(String.format("Not enough money! Requested amount is %s$ but you can't"
                    + "spend more then %s$", amountForSpending, wealth));
        }
    }

    public boolean isSummerMonth() {
        return Month.JUNE.equals(birthdayMonth) || Month.JULY.equals(birthdayMonth)
                || Month.AUGUST.equals(birthdayMonth);
    }

    public boolean isRich() {
        return wealth >= GOOD_WEALTH;
    }

    public boolean isPrettyGirlFriend() {
        return girlFriend != null && girlFriend.isPretty();
    }
}
