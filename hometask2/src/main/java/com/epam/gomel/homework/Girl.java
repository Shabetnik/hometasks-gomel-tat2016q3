package com.epam.gomel.homework;

import lombok.Getter;
import lombok.Setter;

public class Girl extends AbstractHuman {
    @Setter @Getter
    private boolean isPretty;
    @Setter
    private Boy boyFriend;
    @Setter
    private boolean isSlimFriendGotAFewKilos;

    public Girl(boolean isPretty, boolean isSlimFriendGotAFewKilos, Boy boyFriend) {
        this.isPretty = isPretty;
        this.isSlimFriendGotAFewKilos = isSlimFriendGotAFewKilos;
        this.boyFriend = boyFriend;
        if (boyFriend != null) {
            this.boyFriend.setGirlFriend(this);
        }
    }

    public Girl() {
        this(false, false, null);
    }

    @Override
    public Mood getMood() {
        if (isBoyFriendWillBuyNewShoes()) {
            return Mood.EXCELLENT;
        } else if (isPretty() || isBoyfriendRich()) {
            return Mood.GOOD;
        } else if (isSlimFriendBecameFat() ) {
            return Mood.NEUTRAL;
        }
        return Mood.I_HATE_THEM_ALL;
    }

    public void spendBoyFriendMoney(double amountForSpending) {
        if (isBoyfriendRich()) {
            boyFriend.spendSomeMoney(amountForSpending);
        }
    }

    public boolean isBoyfriendRich() {
        return boyFriend != null && boyFriend.isRich();
    }

    public boolean isBoyFriendWillBuyNewShoes() {
        return isBoyfriendRich() && isPretty;
    }

    public boolean isSlimFriendBecameFat() {
        return isSlimFriendGotAFewKilos && !isPretty();
    }

}
