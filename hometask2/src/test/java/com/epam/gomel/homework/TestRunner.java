package com.epam.gomel.homework;

import org.testng.TestNG;

import java.util.Arrays;
import java.util.List;

/**
 * Created by Uladzimir Shabetnik on 22.09.2016.
 */
public class TestRunner {
    public static void main(String[] args) {
        TestNG testNG = new TestNG();
        List<String> files = Arrays.asList(
                "./src/test/resources/suites/boyTest.xml"
        );
        testNG.setTestSuites(files);
        testNG.run();
    }
}
