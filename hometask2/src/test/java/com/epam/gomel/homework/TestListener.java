package com.epam.gomel.homework;

import org.testng.IInvokedMethod;
import org.testng.IInvokedMethodListener;
import org.testng.ITestResult;

/**
 * Created by Uladzimir Shabetnik on 22.09.2016.
 */
public class TestListener implements IInvokedMethodListener {
    @Override
    public void beforeInvocation(IInvokedMethod iInvokedMethod, ITestResult iTestResult) {
        System.out.println("METHOD STARTED:" + iInvokedMethod.getTestMethod().getMethodName());
    }

    @Override
    public void afterInvocation(IInvokedMethod iInvokedMethod, ITestResult iTestResult) {
        System.out.println("SUCCESS:" + iTestResult.isSuccess() + "\nMETHOD COMPLETED");
    }
}
