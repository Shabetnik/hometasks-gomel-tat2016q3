package com.epam.gomel.homework;

import org.testng.Assert;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.CoreMatchers.*;


/**
 * Created by Uladzimir Shabetnik on 21.09.2016.
 */
public class GirlTest {

    private Boy boyfriend;
    private Girl girl;
    public final static double GOOD_WEALTH = 1_000_000;
    public final static double NO_WEALTH = 0;

    @BeforeMethod(description = "Prepare boy&girl")
    public void setUp() {
        boyfriend = new Boy();
        girl = new Girl();
    }

    @Test(description = "If boyfriend is rich and girl is pretty return Mood.EXCELLENT")
    public void testMoodExcellent() throws Exception {
        girl.setPretty(true);
        boyfriend.setWealth(GOOD_WEALTH);
        girl.setBoyFriend(boyfriend);
        Mood actualResult = girl.getMood();
        Assert.assertEquals(actualResult, Mood.EXCELLENT, "Invalid result of testMoodExcellentGirl");
    }

    @DataProvider(name = "data for testMoodGood")
    public Object[][] dataTestMoodGood() {
        return new Object[][]{
                {"Pretty girl"},
                {"Boyfriend is rich"}
        };
    }

    @Test(description = "If boyfriend is rich or girl is pretty return Mood.GOOD", dataProvider = "data for testMoodGood")
    public void testMoodGood(String statement) throws Exception {
        if (statement == "Pretty girl") {
            girl.setPretty(true);
            Mood actualResult = girl.getMood();
            Assert.assertEquals(actualResult, Mood.GOOD, "Invalid result of testMoodGoodGirl");
        } else if (statement == "Boyfriend is rich") {
            boyfriend.setWealth(GOOD_WEALTH);
            girl.setBoyFriend(boyfriend);
            Mood actualResult = girl.getMood();
            Assert.assertEquals(actualResult, Mood.GOOD, "Invalid result of testMoodGoodGirl");
        }
    }

    @Test(description = "If is slim friend became fat true return Mood.NEUTRAL")
    public void testMoodNeutral() throws Exception {
        girl.setSlimFriendGotAFewKilos(true);
        Mood actualResult = girl.getMood();
        Assert.assertEquals(actualResult, Mood.NEUTRAL, "Invalid result of testMoodNeutralGirl");
    }

    @Test(description = "No rich boyfriend not pretty and no is slim friend became fat return Mood.I_HATE_THEM_ALL")
    public void testMoodIHateThemAll() throws Exception {
        Mood actualResult = girl.getMood();
        Assert.assertEquals(actualResult, Mood.I_HATE_THEM_ALL, "Invalid result of testMoodIHateThemAll");
    }

    @Test(description = "If boyfriend is rich must spending him money")
    public void testSpendBoyFriendMoney() throws Exception {
        boyfriend.setWealth(GOOD_WEALTH);
        girl.setBoyFriend(boyfriend);
        boyfriend.setGirlFriend(girl);
        girl.spendBoyFriendMoney(500_000);
        double actualResult = boyfriend.getWealth();
        Assert.assertEquals(actualResult, 500_000D, "Invalid result of testSpendBoyFriendMoney");
    }

    @Test(description = "If boyfriend wealth more then 1 000 000 money return true ")
    public void testIsBoyfriendRich() throws Exception {
        boyfriend.setWealth(GOOD_WEALTH);
        girl.setBoyFriend(boyfriend);
        boolean actualResult = girl.isBoyfriendRich();
        Assert.assertEquals(actualResult, true, "Invalid result of testIsBoyfriendRich");
    }

    @Test(description = "If girl is pretty and boyfriend is rich return true")
    public void testIsBoyFriendWillBuyNewShoes() throws Exception {
        boyfriend.setWealth(GOOD_WEALTH);
        girl.setBoyFriend(boyfriend);
        girl.setPretty(true);
        boolean actualResult = girl.isBoyFriendWillBuyNewShoes();
        //Hamcrest matcher
        assertThat(actualResult, equalTo(true));
    }

    @Test(description = "If boyfriend got few kilos return true")
    public void testIsSlimFriendBecameFat() throws Exception {
        girl.setPretty(false);
        girl.setSlimFriendGotAFewKilos(true);
        boolean actualResult = girl.isSlimFriendBecameFat();
        //Hamcrest matcher
        assertThat(actualResult, equalTo(true));
    }

}