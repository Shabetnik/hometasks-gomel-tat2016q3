package com.epam.gomel.homework;


import org.testng.Assert;
import org.testng.annotations.*;


/**
 * Created by Uladzimir Shabetnik on 21.09.2016.
 */

public class BoyTest {
    private Boy boy;
    private Girl girl;
    public final static double GOOD_WEALTH = 1_000_000;
    public final static double POOR_WEALTH = 100_000;

    @BeforeClass(description = "Prepare boy&girl", groups = "prepare")
    public void setUp() {
        boy = new Boy();
        girl = new Girl();
    }

    @Test(description = "return 'EXCELLENT' if Boy isRich and isPrettyGirlFriend and isSummerMonth", groups = "getMood")
    public void testExcellentGetMood() throws Exception {
        girl.setPretty(true);
        Boy boy = new Boy(Month.AUGUST, GOOD_WEALTH, girl);
        Mood actualResult = boy.getMood();
        Assert.assertEquals(actualResult, Mood.EXCELLENT, "Invalid result of testExcellentGetMoodBoy");
    }

    @Test(description = "return 'GOOD' if Boy isRich and isPrettyGirlFriend", groups = "getMood")
    public void testGoodGetMood() throws Exception {
        girl.setPretty(true);
        Boy boy = new Boy(null, GOOD_WEALTH, girl);
        Mood actualResult = boy.getMood();
        Assert.assertEquals(actualResult, Mood.GOOD, "Invalid result of testGoodGetMoodBoy");
    }

    @Test(description = "return 'NEUTRAL' if Boy isRich and isSummerMonth", groups = "getMood")
    public void testNeutralGetMood() throws Exception {
        Boy boy = new Boy(Month.AUGUST);
        boy.setWealth(GOOD_WEALTH);
        Mood actualResult = boy.getMood();
        Assert.assertEquals(actualResult, Mood.NEUTRAL, "Invalid result of testGoodGetMoodBoy");
    }

    @DataProvider(name = "Data for testBadGetMood")
    public Object[][] dataTestBadGetMood() {
        return new Object[][]{
                {"IsRich", Mood.BAD},
                {"IsPrettyGirlFriend", Mood.BAD},
                {"IsSummerMonth", Mood.BAD},
        };
    }

    @Test(description = "return 'BAD' if Boy isRich or isPrettyGirlFriend or isSummerMonth",
            dataProvider = "Data for testBadGetMood", groups = "getMood")
    public void testBadGetMood(String worth, Mood result) throws Exception {
        if (worth == "IsRich") {
            Boy boy = new Boy(GOOD_WEALTH);
            Mood actualResult = boy.getMood();
            Assert.assertEquals(actualResult, result, "Invalid result of testBadGetMoodBoy");
        } else if (worth == "IsPrettyGirlFriend") {
            girl.setPretty(true);
            Boy boy = new Boy(girl);
            Mood actualResult = boy.getMood();
            Assert.assertEquals(actualResult, result, "Invalid result of testBadGetMoodBoy");
        } else if (worth == "IsSummerMonth") {
            Boy boy = new Boy(Month.AUGUST);
            Mood actualResult = boy.getMood();
            Assert.assertEquals(actualResult, result, "Invalid result of testBadGetMoodBoy");
        }
    }

    @Test(description = "if not summer month and no wealth and no pretty girl return Mood.HORRIBLE ")
    public void testHorribleGetMood() throws Exception {
        Boy boy = new Boy(Month.DECEMBER, POOR_WEALTH, null);
        Mood actualResult = boy.getMood();
        Assert.assertEquals(actualResult, Mood.HORRIBLE, "Invalid result of testGoodGetMoodBoy");
    }


    @Test(description = "Should subtract from wealth money")
    public void testSpendSomeMoney() throws Exception {
        boy.setWealth(500);
        boy.spendSomeMoney(300);
        double actualWealth = boy.getWealth();
        Assert.assertEquals(actualWealth, 200D, "Invalid result of testSpendSomeMoney");
    }

    @Test(description = "if spending money boy greater then wealth return exception",
            expectedExceptions = RuntimeException.class)
    public void testSpendSomeMoneyException() throws Exception {
        boy.setWealth(500);
        boy.spendSomeMoney(600);
        double actualWealth = boy.getWealth();
        Assert.assertEquals(actualWealth, 200D, "Invalid result of testSpendSomeMoneyException");
    }

    @DataProvider(name = "Data for testIsSummerMonth")
    public Object[][] dataTestIsSummerMonth() {
        return new Object[][]{
                {Month.AUGUST, true},
                {Month.JULY, true},
                {Month.JUNE, true},
                {Month.DECEMBER, false},
                {Month.MAY, false},
        };
    }


    @Test(description = "return true if boy birthday in JUNE or JULY or AUGUST",
            dataProvider = "Data for testIsSummerMonth")
    public void testIsSummerMonth(Month month, boolean result) throws Exception {
        boy.setBirthdayMonth(month);
        boolean actualResult = boy.isSummerMonth();
        Assert.assertEquals(actualResult, result);
    }

    @Test(description = "return true if boy wealth more then 1 000 000 money", priority = 1, groups = "rich")
    public void testIsRich() throws Exception {
        boy.setWealth(GOOD_WEALTH);
        boolean actualResult = boy.isRich();
        Assert.assertEquals(actualResult, true, "Invalid result of IsRich");
    }

    @Test(description = "xml test IsRich with parameters", groups = "isRichXML")
    @Parameters({"wealth"})
    public void testIsRichXMLWithParameters(double wealth) throws Exception {
        boy.setWealth(wealth);
        boolean actualResult = boy.isRich();
        Assert.assertEquals(actualResult, true, "Invalid result of testIsRichXMLWithParameters");
    }

    @Test(description = "return true if girl not null and girl isPretty=true", dependsOnGroups = "rich")
    public void testIsPrettyGirlFriend() throws Exception {
        boy.setGirlFriend(girl);
        girl.setBoyFriend(boy);
        girl.setPretty(true);
        boolean actualResult = boy.isPrettyGirlFriend();
        Assert.assertEquals(actualResult, true, "Invalid result of IsPrettyGirlFriend");
    }

}