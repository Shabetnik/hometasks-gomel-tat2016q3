package com.epam.gomel.homework.pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.PageFactory;

/**
 * Created by Uladzimir Shabetnik on 04.10.2016.
 */
public class FileRestoreMessage extends BasePage {
    private static final String FILE_RESTORED_MESSAGE_LOCATOR = "//div[contains(text(),'Файл «%s%s» восстановлен')]";

    public FileRestoreMessage(WebDriver webDriver) {
        super(webDriver);
    }

    public String getFileRestoreSuccessMessage(String filename, String extension) {
        PageFactory.initElements(webDriver, FileRestoreMessage.class);
        waitForLoad(By.xpath(String.format(FILE_RESTORED_MESSAGE_LOCATOR, filename, extension)));
        return webDriver.findElement(By.xpath(
                String.format(FILE_RESTORED_MESSAGE_LOCATOR, filename, extension))).getText();
    }
}
