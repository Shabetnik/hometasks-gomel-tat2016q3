package com.epam.gomel.homework.pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.PageFactory;

/**
 * Created by Uladzimir Shabetnik on 03.10.2016.
 */
public class FilesDeletedSuccessMessage extends BasePage {
    private static final By FILES_DELETED_MESSAGE_LOCATOR = By.xpath("//div[contains(text(),'файла удалены в')]");

    public FilesDeletedSuccessMessage(WebDriver webDriver) {
        super(webDriver);
    }

    public String getFileDeletedPermanentlySuccessMessage() {
        PageFactory.initElements(webDriver, FilesDeletedSuccessMessage.class);
        waitForLoad(FILES_DELETED_MESSAGE_LOCATOR);
        return webDriver.findElement(FILES_DELETED_MESSAGE_LOCATOR).getText();
    }

}
