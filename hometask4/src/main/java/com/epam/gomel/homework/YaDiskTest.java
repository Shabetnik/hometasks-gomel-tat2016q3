package com.epam.gomel.homework;

import com.epam.gomel.homework.webbrowserproperties.BrowserProperties;
import com.epam.gomel.homework.pages.*;
import com.epam.gomel.homework.tool.DownloadFileChecker;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import java.util.concurrent.TimeUnit;

/**
 * Created by Uladzimir Shabetnik on 25.09.2016.
 */
public class YaDiskTest {
    private static final int IMPLICIT_WAIT_IN_SECONDS = 1;
    private static final String INCORRECT_PASSWORD_MAIL = "123";
    private static final String LOGIN_MAIL = "ushabetnik";
    private static final String PASSWORD_MAIL = "sail6270109";
    private static final int PAGE_LOAD_TIME = 30;
    private static final String FILE_DELETED_PERMANENTLY_SUCCESS_MESSAGE = "Файл «%s%s» был удален";
    private static final String FILE_REMOVE_TO_TRASH_SUCCESS_MESSAGE = "Файл «%s%s» удален в Корзину";
    private static final String FILE_RESTORE_FROM_TRASH_SUCCESS_MESSAGE = "Файл «%s%s» восстановлен";
    private static final String FILES_REMOVE_TO_TRASH_SUCCESS_MESSAGE = "2 файла удалены в Корзину";
    private static final int TIME_FOR_WAITING_ARRIVE_FILE = 2000;
    private WebDriver webDriver;
    private String filename;
    private String extension;

    @BeforeClass
    public void setUpDriver() {
        BrowserProperties browserProperties = new BrowserProperties();
        webDriver = new ChromeDriver(browserProperties.getChromeProperties());
        //webDriver = new FirefoxDriver(browserProperties.getFirefoxProperties());
        webDriver.manage().timeouts().implicitlyWait(IMPLICIT_WAIT_IN_SECONDS, TimeUnit.SECONDS);
        webDriver.manage().timeouts().pageLoadTimeout(PAGE_LOAD_TIME, TimeUnit.SECONDS);
        webDriver.manage().window().maximize();
        webDriver.get("https://disk.yandex.ru/");
        filename = "upload";
        extension = ".txt";
    }

//    @Test(description = "Must go to page with error when login and password are incorrect")
//    private void loginErrorPageTest() {
//        LoginPage loginPage = new LoginPage(webDriver);
//        LoginErrorMessagePage loginErrorMessagePage = loginPage.incorrectLogin(LOGIN_MAIL, INCORRECT_PASSWORD_MAIL);
//        Assert.assertTrue(loginErrorMessagePage.isErrorLoginPage(), "loginErrorPageTest error");
//    }

    @Test(description = "Must go in account with correct login and password")
    public void loginCorrectTest() {
        //webDriver.get("https://disk.yandex.ru/");
        LoginPage loginPage = new LoginPage(webDriver);
        loginPage.login(LOGIN_MAIL, PASSWORD_MAIL);
        LoginSuccessMessagePage loginSuccessMessagePage = new LoginSuccessMessagePage(webDriver);
        Assert.assertEquals(loginSuccessMessagePage.getCurrentUser(), LOGIN_MAIL, "loginCorrectTest Error");
    }

    @Test(description = "Must upload file to yandex disk", dependsOnMethods = "loginCorrectTest")
    public void uploadFileTest() {
        FileActionPage fileActionPage = new FileActionPage(webDriver);
        FileUploadSuccessMessage fileUploadSuccessMessage = fileActionPage.uploadFile(filename, extension);
        Assert.assertTrue(fileUploadSuccessMessage.isFileUploadedSuccess(filename), "uploadFileTest error");
    }

    @Test(description = "Must download file from yandex disk and check success downloaded",
            dependsOnMethods = "uploadFileTest")
    public void downloadFileTest() throws InterruptedException {
        FileActionPage fileActionPage = new FileActionPage(webDriver);
        fileActionPage.downloadFile(filename);
        Thread.sleep(TIME_FOR_WAITING_ARRIVE_FILE);
        DownloadFileChecker downloadFileChecker = new DownloadFileChecker();
        Assert.assertTrue(downloadFileChecker.isFileDownloaded(filename + extension));
    }

    @Test(description = "Must click and hold on file and move it to trash", dependsOnMethods = "downloadFileTest")
    public void clickOnFileAndMoveItToTrashTest() {
        FileActionPage fileActionPage = new FileActionPage(webDriver);
        FileToTrashSuccessMessage fileToTrashSuccessMessage = fileActionPage.removeFileToTrash(filename);
        String fileRemovedToTrashResult = String.format(FILE_REMOVE_TO_TRASH_SUCCESS_MESSAGE, filename, extension);
        Assert.assertEquals(fileToTrashSuccessMessage.getFileToTrashMessage(filename, extension),
                fileRemovedToTrashResult, "clickOnFileAndMoveItToTrashTest error");
    }

    @Test(description = "Must restore file from trash", dependsOnMethods = "clickOnFileAndMoveItToTrashTest")
    public void restoreFileTest() {
        FileActionPage fileActionPage = new FileActionPage(webDriver);
        FileRestoreMessage fileRestoreMessage = fileActionPage.restoreFileFromTrash(filename);
        String fileRestoreResult = String.format(FILE_RESTORE_FROM_TRASH_SUCCESS_MESSAGE, filename, extension);
        Assert.assertEquals(fileRestoreMessage.getFileRestoreSuccessMessage(filename, extension),
                fileRestoreResult, "restoreFileTest error ");
    }

    @Test(description = "Must permanently delete file from trash permanently", dependsOnMethods = "restoreFileTest")
    public void deleteFilePermanentlyTest() {
        FileActionPage fileActionPage = new FileActionPage(webDriver);
        fileActionPage.enterToFileSection();
        fileActionPage.removeFileToTrash(filename);
        FileDeletedSuccessMessage fileDeletedSuccessMessage = fileActionPage.removeFilePermanently(filename);
        String fileDeletedResult = String.format(FILE_DELETED_PERMANENTLY_SUCCESS_MESSAGE, filename, extension);
        Assert.assertEquals(fileDeletedSuccessMessage.getFileDeletedPermanentlySuccessMessage(filename, extension),
                fileDeletedResult, "deleteFilePermanentlyTest error ");
    }

    @Test(description = "Must remove to trash several files", dependsOnMethods = "deleteFilePermanentlyTest")
    public void removeseveralFilesToTrashTest() {
        String filename1 = "Горы";
        String filename2 = "Москва";
        FileActionPage fileActionPage = new FileActionPage(webDriver);
        FilesDeletedSuccessMessage filesDeletedSuccessMessage = fileActionPage.removeSeveralFiles(filename1, filename2);
        Assert.assertEquals(filesDeletedSuccessMessage.getFileDeletedPermanentlySuccessMessage(),
                FILES_REMOVE_TO_TRASH_SUCCESS_MESSAGE, "removeseveralFilesToTrashTest error");
    }

    @AfterClass
    public void driverQuit() {
        webDriver.quit();
    }
}
