package com.epam.gomel.homework.webbrowserproperties;

import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.firefox.FirefoxProfile;
import org.openqa.selenium.remote.CapabilityType;
import org.openqa.selenium.remote.DesiredCapabilities;

import java.util.HashMap;

/**
 * Created by Uladzimir Shabetnik on 03.10.2016.
 */
public class BrowserProperties {
    private static final String DOWNLOAD_PATH = "D:\\";

    public FirefoxProfile getFirefoxProperties() {
        FirefoxProfile firefoxProfile = new FirefoxProfile();
        firefoxProfile.setPreference("browser.download.folderList", 2);
        firefoxProfile.setPreference("browser.download.manager.showWhenStarting", false);
        firefoxProfile.setPreference("browser.download.dir", DOWNLOAD_PATH);
        firefoxProfile.setPreference("browser.helperApps.neverAsk.saveToDisk", "text/plain,image/jpeg");
        return firefoxProfile;
    }

    public DesiredCapabilities getChromeProperties() {
        HashMap<String, Object> chromePrefs = new HashMap<>();
        chromePrefs.put("profile.default_content_settings.popups", 0);
        chromePrefs.put("download.default_directory", DOWNLOAD_PATH);
        ChromeOptions options = new ChromeOptions();
        options.setExperimentalOption("prefs", chromePrefs);
        DesiredCapabilities capabilities = DesiredCapabilities.chrome();
        capabilities.setCapability(ChromeOptions.CAPABILITY, options);
        capabilities.setCapability(CapabilityType.ACCEPT_SSL_CERTS, true);
        return capabilities;
    }

}
