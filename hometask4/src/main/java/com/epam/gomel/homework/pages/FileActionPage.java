package com.epam.gomel.homework.pages;

import org.openqa.selenium.*;
import org.openqa.selenium.interactions.Actions;

import java.io.File;

/**
 * Created by Uladzimir Shabetnik on 03.10.2016.
 */
public class FileActionPage extends BasePage {
    private static final By UPLOAD_FILE_LOCATOR = By.xpath("//input[@type='file']");
    private static final By BUTTON_CLOSE_AFTER_UPLOAD_LOCATOR = By.xpath("//a[@data-click-action='dialog.close']");
    private static final String LOCATION_UPLOAD_FILE = "./src/main/resources/%s%s";
    private static final String FILE_NAME_LOCATOR = "//div[contains(text(),'%s')]";
    private static final By TRASH_LOCATOR = By.xpath("//div[@title='Корзина']");
    private static final By BUTTON_DOWNLOAD_LOCATOR = By.xpath("//button[@data-click-action='resource.download']");
    private static final By BUTTON_PERMANENTLY_DELETE_LOCATOR =
            By.xpath("//button[@data-click-action='resource.delete']");
    private static final By BUTTON_RESTORE_LOCATOR = By.xpath("//button[@data-click-action='resource.restore']");
    private static final By PROGRESS_BAR_LOCATOR = By.xpath("//div[@class='b-progressbar__fill']");
    private static final By FILE_SECTION_LOCATOR = By.xpath("//a[@href='/client/disk']");

    public FileActionPage(WebDriver webDriver) {
        super(webDriver);
    }

    public void enterToFileSection() {
        webDriver.findElement(FILE_SECTION_LOCATOR).click();
    }

    public void enterToTrash() {
        waitForLoad(TRASH_LOCATOR);
        WebElement trash = webDriver.findElement(TRASH_LOCATOR);
        Actions actions = new Actions(webDriver);
        actions.doubleClick(trash).build().perform();
        waitForInvisibility(PROGRESS_BAR_LOCATOR);
    }

    public void clickOnFile(String filename) {
        waitForLoad(By.xpath(String.format(FILE_NAME_LOCATOR, filename)));
        webDriver.findElement(By.xpath(String.format(FILE_NAME_LOCATOR, filename))).click();
    }

    public FileUploadSuccessMessage uploadFile(String filename, String extension) {
        waitElementOnPage(UPLOAD_FILE_LOCATOR);
        webDriver.findElement(UPLOAD_FILE_LOCATOR).sendKeys(
                new File(String.format(LOCATION_UPLOAD_FILE, filename, extension)).getAbsolutePath());
        webDriver.findElement(BUTTON_CLOSE_AFTER_UPLOAD_LOCATOR).click();
        return new FileUploadSuccessMessage(webDriver);
    }

    public void downloadFile(String filename) {
        clickOnFile(filename);
        waitForLoad(BUTTON_DOWNLOAD_LOCATOR);
        webDriver.findElement(BUTTON_DOWNLOAD_LOCATOR).click();
    }

    public FileToTrashSuccessMessage removeFileToTrash(String filename) {
        Actions actions = new Actions(webDriver);
        waitForLoad(TRASH_LOCATOR);
        WebElement trash = webDriver.findElement(TRASH_LOCATOR);
        WebElement file = webDriver.findElement(By.xpath(String.format(FILE_NAME_LOCATOR, filename)));
        actions.clickAndHold(file);
        actions.moveToElement(trash);
        actions.release().build().perform();
        return new FileToTrashSuccessMessage(webDriver);
    }

    public FileDeletedSuccessMessage removeFilePermanently(String filename) {
        enterToTrash();
        clickOnFile(filename);
        waitForLoad(BUTTON_PERMANENTLY_DELETE_LOCATOR);
        webDriver.findElement(BUTTON_PERMANENTLY_DELETE_LOCATOR).click();
        waitForInvisibility(PROGRESS_BAR_LOCATOR);
        enterToFileSection();
        return new FileDeletedSuccessMessage(webDriver);
    }

    public FileRestoreMessage restoreFileFromTrash(String filename) {
        removeFileToTrash(filename);
        enterToTrash();
        clickOnFile(filename);
        webDriver.findElement(BUTTON_RESTORE_LOCATOR).click();
        waitForInvisibility(PROGRESS_BAR_LOCATOR);
        webDriver.findElement(FILE_SECTION_LOCATOR).click();
        return new FileRestoreMessage(webDriver);
    }

    public FilesDeletedSuccessMessage removeSeveralFiles(String filenameFirst, String filenameSecond) {
        waitForLoad(By.xpath(String.format(FILE_NAME_LOCATOR, filenameFirst)));
        WebElement firstFile = webDriver.findElement(By.xpath(String.format(FILE_NAME_LOCATOR, filenameFirst)));
        WebElement secondFile = webDriver.findElement(By.xpath(String.format(FILE_NAME_LOCATOR, filenameSecond)));
        WebElement trash = webDriver.findElement(TRASH_LOCATOR);
        Actions actions = new Actions(webDriver);
        actions.click(firstFile).keyDown(Keys.CONTROL).click(secondFile).keyUp(Keys.CONTROL).build().perform();
        actions.clickAndHold(firstFile).moveToElement(trash).release().build().perform();
        return new FilesDeletedSuccessMessage(webDriver);
    }

}
