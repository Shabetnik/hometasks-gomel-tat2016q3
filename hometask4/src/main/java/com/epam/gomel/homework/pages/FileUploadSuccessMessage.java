package com.epam.gomel.homework.pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.PageFactory;

/**
 * Created by Uladzimir Shabetnik on 04.10.2016.
 */
public class FileUploadSuccessMessage extends BasePage {
    private static final String FILE_NAME_LOCATOR = "//div[contains(text(),'%s')]";
    private static final By SUCCESS_UPLOAD_FILE_MESSAGE = By.xpath("//*[contains(text(),'Все загрузки завершены')]");

    public FileUploadSuccessMessage(WebDriver webDriver) {
        super(webDriver);
    }

    public boolean isFileUploadedSuccess(String filename) {
        PageFactory.initElements(webDriver, FileUploadSuccessMessage.class);
        waitForLoad(SUCCESS_UPLOAD_FILE_MESSAGE);
        waitForLoad(By.xpath(String.format(FILE_NAME_LOCATOR, filename)));
        return webDriver.findElement(By.xpath(String.format(FILE_NAME_LOCATOR, filename))).isDisplayed();
    }
}
