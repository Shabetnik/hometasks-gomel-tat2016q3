package com.epam.gomel.homework.pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.PageFactory;

/**
 * Created by Uladzimir Shabetnik on 26.09.2016.
 */
public class LoginErrorMessagePage extends BasePage {
    private static final By LOGIN_FAILED_LOCATOR = By.xpath("//label["
            + "@class='nb-input _nb-complex-input nb-form-auth__input _init _nb-is-wrong']");

    public LoginErrorMessagePage(WebDriver webDriver) {
        super(webDriver);
    }

    public boolean isErrorLoginPage() {
        PageFactory.initElements(webDriver, LoginErrorMessagePage.class);
        waitForLoad(LOGIN_FAILED_LOCATOR);
        return webDriver.findElement(LOGIN_FAILED_LOCATOR).isDisplayed();
    }
}
