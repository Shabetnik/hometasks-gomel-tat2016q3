package com.epam.gomel.homework.pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.PageFactory;

/**
 * Created by Uladzimir Shabetnik on 26.09.2016.
 */
public class LoginSuccessMessagePage extends BasePage {
    private static final By LOGIN_SUCCESS_LOCATOR = By.xpath("//span[@class='header__username']");

    public LoginSuccessMessagePage(WebDriver webDriver) {
        super(webDriver);
    }

    public String getCurrentUser() {
        PageFactory.initElements(webDriver, LoginSuccessMessagePage.class);
        waitForLoad(LOGIN_SUCCESS_LOCATOR);
        return webDriver.findElement(LOGIN_SUCCESS_LOCATOR).getText();
    }
}
