package com.epam.gomel.homework.pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.PageFactory;

/**
 * Created by Uladzimir Shabetnik on 03.10.2016.
 */
public class FileToTrashSuccessMessage extends BasePage {
    private static final String FILE_TO_TRASH_MESSAGE_LOCATOR = "//div[contains(text(),'Файл «%s%s» удален в')]";

    public FileToTrashSuccessMessage(WebDriver webDriver) {
        super(webDriver);
    }

    public String getFileToTrashMessage(String filename, String extension) {
        PageFactory.initElements(webDriver, FileToTrashSuccessMessage.class);
        waitForLoad(By.xpath(String.format(FILE_TO_TRASH_MESSAGE_LOCATOR, filename, extension)));
        return webDriver.findElement(By.xpath(
                String.format(FILE_TO_TRASH_MESSAGE_LOCATOR, filename, extension))).getText();
    }
}
