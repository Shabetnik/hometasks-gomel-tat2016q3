package com.epam.gomel.homework.pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.PageFactory;

/**
 * Created by Uladzimir Shabetnik on 03.10.2016.
 */
public class FileDeletedSuccessMessage extends BasePage {
    private static final String FILE_DELETED_MESSAGE_LOCATOR = "//div[contains(text(),'Файл «%s%s» был удален')]";

    public FileDeletedSuccessMessage(WebDriver webDriver) {
        super(webDriver);
    }

    public String getFileDeletedPermanentlySuccessMessage(String filename, String extension) {
        PageFactory.initElements(webDriver, FileDeletedSuccessMessage.class);
        waitElementOnPage(By.xpath(String.format(FILE_DELETED_MESSAGE_LOCATOR, filename, extension)));
        return webDriver.findElement(By.xpath(String.
                format(FILE_DELETED_MESSAGE_LOCATOR, filename, extension))).getText();
    }

}
