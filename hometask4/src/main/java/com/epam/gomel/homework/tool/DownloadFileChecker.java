package com.epam.gomel.homework.tool;

import java.io.File;

/**
 * Created by Uladzimir Shabetnik on 03.10.2016.
 */
public class DownloadFileChecker {
    private static final String DOWNLOAD_PATH = "D:\\";

    public boolean isFileDownloaded(String fileName) {
        boolean flag = false;
        File dir = new File(DOWNLOAD_PATH);
        File[] dirContents = dir.listFiles();

        for (int i = 0; i < dirContents.length; i++) {
            if (dirContents[i].getName().equals(fileName)) {
                return flag = true;
            }
        }
        return flag;
    }
}
