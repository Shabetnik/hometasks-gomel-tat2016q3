package com.epam.gomel.homework.pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

/**
 * Created by Uladzimir Shabetnik on 03.10.2016.
 */
public class BasePage {
    private static final int WEB_DRIVER_WAIT_IN_SECOND = 30;
    WebDriver webDriver;

    public BasePage(WebDriver webDriver) {
        this.webDriver = webDriver;
    }

    public void waitForLoad(By locator) {
        WebDriverWait wait = new WebDriverWait(webDriver, WEB_DRIVER_WAIT_IN_SECOND);
        wait.until(ExpectedConditions.visibilityOfElementLocated(locator));
    }

    public void waitForInvisibility(By locator) {
        WebDriverWait wait = new WebDriverWait(webDriver, WEB_DRIVER_WAIT_IN_SECOND);
        wait.until(ExpectedConditions.invisibilityOfElementLocated(locator));
    }

    public void waitElementOnPage(By locator) {
        WebDriverWait wait = new WebDriverWait(webDriver, WEB_DRIVER_WAIT_IN_SECOND);
        wait.until(ExpectedConditions.presenceOfAllElementsLocatedBy(locator));
    }

}
