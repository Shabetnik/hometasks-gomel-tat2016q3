package com.epam.gomel.homework.pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

/**
 * Created by Uladzimir Shabetnik on 25.09.2016.
 */
public class LoginPage extends BasePage {
    private static final By LOGIN_INPUT_LOCATOR = By.xpath("//input[@name='login']");
    private static final By PASSWORD_INPUT_LOCATOR = By.xpath("//input[@name='password']");
    private static final By BUTTON_LOGIN_LOCATOR = By.xpath("//div[@class='nb-input-group']//button");

    public LoginPage(WebDriver webDriver) {
        super(webDriver);
    }

    public LoginSuccessMessagePage correctLogin(String login, String password) {
        webDriver.findElement(LOGIN_INPUT_LOCATOR).sendKeys(login);
        webDriver.findElement(PASSWORD_INPUT_LOCATOR).sendKeys(password);
        webDriver.findElement(BUTTON_LOGIN_LOCATOR).click();
        return new LoginSuccessMessagePage(webDriver);
    }

    public void login(String login, String password) {
        correctLogin(login, password);
    }

    public LoginErrorMessagePage incorrectLogin(String login, String password) {
        correctLogin(login, password);
        return new LoginErrorMessagePage(webDriver);
    }

}
