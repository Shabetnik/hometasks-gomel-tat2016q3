package com.epam.gomel.homework.framework.logging;

import org.apache.log4j.Logger;

/**
 * Created by Uladzimir Shabetnik on 13.10.2016.
 */
public class Log {
    private static final Logger LOGGER = Logger.getLogger("com.epam.gomel.homework");

    public static void info(Object message) {
        LOGGER.info(message);
    }

    public static void error(Object error, Throwable throwable) {
        LOGGER.error(error, throwable);
    }

    public static void debug(Object message) {
        LOGGER.debug(message);
    }

    public static void warn(Object message) {
        LOGGER.warn(message);
    }

}
