package com.epam.gomel.homework.framework.ui.element;

import com.epam.gomel.homework.framework.ui.browser.Browser;
import lombok.Getter;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.Select;

import java.io.File;

/**
 * Created by Uladzimir Shabetnik on 08.10.2016.
 */
public class Element {
    @Getter
    private By by;

    public Element(By by) {
        this.by = by;
    }

    public WebElement getWrappedWebElement() {
        return Browser.getBrowser().findElement(by);
    }

    public void typeValue(String value) {
        WebElement webElement = getWrappedWebElement();
        webElement.clear();
        webElement.sendKeys(value);
    }

    public void click() {
        getWrappedWebElement().click();
    }

    public void chooseDropDownBox(Element element, String selectValue) {
        Select select = new Select(element.getWrappedWebElement());
        select.selectByVisibleText(selectValue);
    }

    public boolean isVisible() {
        return Browser.getBrowser().isElementVisible(by);
    }

    public String getText() {
        return getWrappedWebElement().getText();
    }

    public void waitForVisible() {
        Browser.getBrowser().waitForElementIsVisible(by);
    }

    public void waitForAppear() {
        Browser.getBrowser().waitForElementIsAppear(by);
    }

    public void waitForClickable() {
        Browser.getBrowser().waitForElementIsClickable(by);
    }

    public void waitForDisappear() {
        Browser.getBrowser().waitForElementIsInvisible(by);
    }

    public void switchToFrame() {
        Browser.getBrowser().switchToFrameElement(getWrappedWebElement());
    }

    public void switchToDefaultFrame() {
        Browser.getBrowser().switchToDefaultFame();
    }

    public void typeFile(String path) {
        getWrappedWebElement().findElement(by).sendKeys(new File(path).getAbsolutePath());
    }

}
