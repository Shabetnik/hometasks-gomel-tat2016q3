package com.epam.gomel.homework.product.aliexpress.purchase.page.watches;

import com.epam.gomel.homework.framework.ui.element.Element;
import org.openqa.selenium.By;

/**
 * Created by Uladzimir Shabetnik on 29.10.2016.
 */
public class AliContentWatchesPage {
    private static final By FIRST_WATCHES_FROM_LIST = By.xpath("//li[1][@class='list-item list-item-first ']");
    private Element firstWatches = new Element(FIRST_WATCHES_FROM_LIST);

    public AliContentWatchesPage chooseFirstWatches() {
        firstWatches.click();
        return this;
    }
}
