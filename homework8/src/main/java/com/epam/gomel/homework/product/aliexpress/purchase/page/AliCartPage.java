package com.epam.gomel.homework.product.aliexpress.purchase.page;

import com.epam.gomel.homework.framework.ui.element.Element;
import org.openqa.selenium.By;

/**
 * Created by Uladzimir Shabetnik on 29.10.2016.
 */
public class AliCartPage {
    private static final By DELETE_PURCHASE_BUTTON = By.xpath("//a[@class='remove-single-product']");
    private static final By EMPTY_CART = By.xpath("//div[@id='notice']");

    private Element deletePurchaseButton = new Element(DELETE_PURCHASE_BUTTON);
    private Element emptyCart = new Element(EMPTY_CART);

    public void deletePurchase() {
        deletePurchaseButton.waitForVisible();
        deletePurchaseButton.click();
    }

    public boolean checkDeletedPurchaseFromCart() {
        emptyCart.waitForAppear();
        return true;
    }
}
