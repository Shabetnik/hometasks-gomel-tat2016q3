package com.epam.gomel.homework.framework.exception;

/**
 * Created by Uladzimir Shabetnik on 08.10.2016.
 */
public class CommonTestRuntimeException extends RuntimeException {

    public CommonTestRuntimeException(String message) {
        super(message);
    }

    public CommonTestRuntimeException(String message, Throwable cause) {
        super(message, cause);
    }
}
