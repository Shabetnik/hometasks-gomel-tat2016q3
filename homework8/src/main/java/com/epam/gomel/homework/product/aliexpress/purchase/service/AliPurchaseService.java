package com.epam.gomel.homework.product.aliexpress.purchase.service;

import com.epam.gomel.homework.product.aliexpress.purchase.page.AliCartPage;
import com.epam.gomel.homework.product.aliexpress.purchase.page.AliHeaderMenuPage;
import com.epam.gomel.homework.product.aliexpress.purchase.page.AliPurchasePage;
import com.epam.gomel.homework.product.aliexpress.purchase.page.watches.AliBrandWatchesPage;
import com.epam.gomel.homework.product.aliexpress.purchase.page.watches.AliContentWatchesPage;

/**
 * Created by Uladzimir Shabetnik on 29.10.2016.
 */
public class AliPurchaseService {
    private AliHeaderMenuPage aliHeaderMenuPage = new AliHeaderMenuPage();
    private AliBrandWatchesPage aliBrandWatchesPage = new AliBrandWatchesPage();
    private AliContentWatchesPage aliContentWatchesPage = new AliContentWatchesPage();
    private AliPurchasePage aliPurchasePage = new AliPurchasePage();
    private AliCartPage aliCartPage = new AliCartPage();

    public void chooseSearchSection(String category) {
        aliPurchasePage.closeWelcomePopup();
        aliHeaderMenuPage.clickOnSearchSectionMenu().chooseWatchSection(category).clickSearchButton();
    }

    public void chooseBrandWatches() {
        aliBrandWatchesPage.chooseSkmeiBrandWatches();
    }

    public void chooseFirstWatches() {
        aliContentWatchesPage.chooseFirstWatches();
    }

    public void addPurchaseToCart() {
        aliPurchasePage.chooseFirstColor().addToCart().closePurchasePopup();
    }

    public void enterToCartSection() {
        aliHeaderMenuPage.clickOnCartSection();
    }

    public void deletePurchaseFromCart() {
        aliCartPage.deletePurchase();
    }
}
