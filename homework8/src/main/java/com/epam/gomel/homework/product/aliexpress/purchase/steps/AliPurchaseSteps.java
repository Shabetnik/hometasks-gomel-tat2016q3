package com.epam.gomel.homework.product.aliexpress.purchase.steps;

import com.epam.gomel.homework.framework.logging.Log;
import com.epam.gomel.homework.framework.ui.browser.Browser;
import com.epam.gomel.homework.product.aliexpress.purchase.page.AliCartPage;
import com.epam.gomel.homework.product.aliexpress.purchase.page.AliNavigationSearchResultPage;
import com.epam.gomel.homework.product.aliexpress.purchase.page.AliPurchasePage;
import com.epam.gomel.homework.product.aliexpress.purchase.page.watches.AliBrandWatchesPage;
import com.epam.gomel.homework.product.aliexpress.purchase.service.AliPurchaseService;
import org.jbehave.core.annotations.*;
import org.testng.Assert;

/**
 * Created by Uladzimir Shabetnik on 29.10.2016.
 */
public class AliPurchaseSteps {
    private static final String ALI_URL_HOMEPAGE = "https://ru.aliexpress.com/";
    private static final String WATCHES_SECTION = "Часы";
    private AliPurchaseService aliPurchaseService = new AliPurchaseService();
    private AliPurchasePage aliPurchasePage = new AliPurchasePage();
    private AliCartPage aliCartPage = new AliCartPage();
    private AliNavigationSearchResultPage aliNavigationSearchResultPage = new AliNavigationSearchResultPage();
    private AliBrandWatchesPage aliBrandWatchesPage = new AliBrandWatchesPage();

    @Given("be on page https://ru.aliexpress.com")
    public void openAliPage() {
        Log.info("openAliPage");
        Browser.getBrowser().open(ALI_URL_HOMEPAGE);
    }

    @When("I choose product <category> and click search button")
    public void chooseProductCategory(@Named("category") String category) {
        Log.info("chooseProductCategory");
        aliPurchaseService.chooseSearchSection(category);
    }

    @Then("I get page <result> with all products from needed category")
    public void checkResultOfSearchCategory(@Named("result") String result) {
        Log.info("checkResultOfSearchCategory");
        String actualCategory = aliNavigationSearchResultPage.getCurrentCategory();
        String expectedCategory = result;
        Assert.assertEquals(actualCategory, expectedCategory, "checkResultOfSearchCategory error");
        Browser.getBrowser().close();
    }

    @Given("be on search result of watches page")
    public void openResultWatchesPage() {
        Log.info("openResultWatchesPage");
        Browser.getBrowser().open(ALI_URL_HOMEPAGE);
        aliPurchaseService.chooseSearchSection(WATCHES_SECTION);
    }

    @When("I click on Skmei Brand category")
    public void chooseWatchesBrand() {
        Log.info("chooseWatchesBrand");
        aliPurchaseService.chooseBrandWatches();
    }

    @Then("I get only watches with this brand")
    public void checkNeededWatchesBrand() {
        Log.info("checkNeededWatchesBrand");
        Assert.assertTrue(aliBrandWatchesPage.isSkmeiBrandWatchesSelected());
        Browser.getBrowser().close();
    }

    @When("I click on any watches")
    public void clickOnFirstWatches() {
        Log.info("clickOnFirstWatches");
        aliPurchaseService.chooseFirstWatches();
    }

    @Then("I get description page needed watches")
    public void checkNeededPageContent() {
        Log.info("checkNeededPageContent");
        Assert.assertTrue(aliPurchasePage.isOnPurchasePage());
        Browser.getBrowser().close();
    }

    @Given("be on description page needed watches")
    public void enterToPurchasePage() {
        Browser.getBrowser().open(ALI_URL_HOMEPAGE);
        aliPurchaseService.chooseSearchSection(WATCHES_SECTION);
        aliPurchaseService.chooseBrandWatches();
        aliPurchaseService.chooseFirstWatches();
    }

    @When("I click Add button watches")
    public void addPurchaseToCart() {
        Log.info("checkNeededPageContent");
        aliPurchaseService.addPurchaseToCart();
    }

    @Then("I get needed watches in my Cart")
    public void checkAddedWatchesToCart() {
        Log.info("checkAddedWatchesToCart");
        Assert.assertTrue(aliPurchasePage.checkAddedPurchaseToCart());
    }

    @Given("be on Cart page")
    public void enterToCartSection() {
        Log.info("enterToCartSection");
        aliPurchaseService.enterToCartSection();
    }

    @When("I click Delete button watches")
    public void deletePurchaseFromCart() {
        Log.info("deletePurchaseFromCart");
        aliPurchaseService.deletePurchaseFromCart();
    }

    @Then("I get empty Cart section")
    public void checkPurchaseDeleted() {
        Log.info("checkPurchaseDeleted");
        Assert.assertTrue(aliCartPage.checkDeletedPurchaseFromCart());
        Browser.getBrowser().close();
    }
}
