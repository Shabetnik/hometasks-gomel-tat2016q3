package com.epam.gomel.homework.product.aliexpress.purchase.page.watches;

import com.epam.gomel.homework.framework.ui.element.Element;
import org.openqa.selenium.By;

/**
 * Created by Uladzimir Shabetnik on 29.10.2016.
 */
public class AliBrandWatchesPage {
    private static final By SKMEI_BRAND = By.xpath("//a[@title='Skmei']");
    private static final By SKMEI_BRAND_SELECTED
            = By.xpath("//li[@class='brand-content  selected ']//a[@title='Skmei']");

    private Element chooseSkmeiBrand = new Element(SKMEI_BRAND);
    private Element brandSkmeiSelect = new Element(SKMEI_BRAND_SELECTED);

    public AliBrandWatchesPage chooseSkmeiBrandWatches() {
        chooseSkmeiBrand.click();
        return this;
    }

    public boolean isSkmeiBrandWatchesSelected() {
        brandSkmeiSelect.waitForAppear();
        return brandSkmeiSelect.isVisible();
    }
}
