package com.epam.gomel.homework.framework.listener;

import com.epam.gomel.homework.framework.logging.Log;
import com.epam.gomel.homework.framework.ui.browser.Browser;
import org.testng.ITestResult;
import org.testng.TestListenerAdapter;

/**
 * Created by Uladzimir Shabetnik on 08.10.2016.
 */
public class TestListener extends TestListenerAdapter {

    @Override
    public void onTestStart(ITestResult tr) {
        Log.info(tr.getTestClass().getName() + "." + tr.getName() + " IS STARTING");
    }

    @Override
    public void onTestSuccess(ITestResult tr) {
        Log.info(tr.getTestClass().getName() + "." + tr.getName() + " PASSED");
        Browser.getBrowser().close();
    }

    @Override
    public void onTestFailure(ITestResult tr) {
        Log.error(tr.getTestClass().getName()
                + "." + tr.getName() + " failed", new Throwable(tr.getThrowable().getMessage()));
        Browser.getBrowser().close();
    }

    public void onTestSkipped(ITestResult tr) {
        Log.warn(tr.getTestClass().getName() + "." + tr.getName() + " SKIPPED");
        Browser.getBrowser().close();
    }

}
