package com.epam.gomel.homework.product.aliexpress.purchase.page;

import com.epam.gomel.homework.framework.ui.element.Element;
import org.openqa.selenium.By;

/**
 * Created by Uladzimir Shabetnik on 29.10.2016.
 */
public class AliPurchasePage {
    private static final By FIRST_COLOR_FOR_WATCHES = By.xpath("//li[1][@class='item-sku-image']");
    private static final By ADD_TO_CART_BUTTON = By.xpath("//a[@id='j-add-cart-btn']");
    private static final By CLOSE_POPUP_PURCHASE_BUTTON = By.xpath("//a[@class='ui-window-close']");
    private static final By CLOSE_WELCOME_POPUP_BUTTON = By.xpath("//a[@class='close-layer']");
    private static final By PURCHASE_SUCCESS_ADDED_TO_CART = By.xpath("//h3[@class='ui-feedback-header']");

    private Element firstColorWatches = new Element(FIRST_COLOR_FOR_WATCHES);
    private Element addToCartButton = new Element(ADD_TO_CART_BUTTON);
    private Element closePurchasePopupButton = new Element(CLOSE_POPUP_PURCHASE_BUTTON);
    private Element closeWelcomePopupButton = new Element(CLOSE_WELCOME_POPUP_BUTTON);
    private Element purchaseAddedToCart = new Element(PURCHASE_SUCCESS_ADDED_TO_CART);

    public AliPurchasePage chooseFirstColor() {
        firstColorWatches.click();
        return this;
    }

    public AliPurchasePage addToCart() {
        addToCartButton.click();
        return this;
    }

    public boolean isOnPurchasePage() {
        return addToCartButton.isVisible();
    }

    public AliPurchasePage closePurchasePopup() {
        closePurchasePopupButton.click();
        closePurchasePopupButton.waitForDisappear();
        return this;
    }

    public AliPurchasePage closeWelcomePopup() {
        closeWelcomePopupButton.waitForVisible();
        closeWelcomePopupButton.click();
        return this;
    }

    public boolean checkAddedPurchaseToCart() {
        boolean purchaseSuccessAdded = purchaseAddedToCart.isVisible();
        closePurchasePopup();
        return purchaseSuccessAdded;
    }
}
