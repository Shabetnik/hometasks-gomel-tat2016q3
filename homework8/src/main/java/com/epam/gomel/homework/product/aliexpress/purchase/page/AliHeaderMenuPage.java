package com.epam.gomel.homework.product.aliexpress.purchase.page;

import com.epam.gomel.homework.framework.ui.element.Element;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriverException;

/**
 * Created by Uladzimir Shabetnik on 29.10.2016.
 */
public class AliHeaderMenuPage {
    private static final String WATCHES_SECTION = "Часы";
    private static final By SEARCH_BUTTON = By.xpath("//input[@class='search-button']");
    private static final By DROPDOWN_SELECT = By.xpath("//select[@id='search-dropdown-box']");
    private static final By SEARCH_SECTION_BUTTON = By.xpath("//div[@id='search-cate']");
    private static final By CART_SECTION_BUTTON
            = By.xpath("//a[@rel='nofollow' and @href='//shoppingcart.aliexpress.com/shopcart/shopcartDetail.htm']");

    private Element dropDownSelect = new Element(DROPDOWN_SELECT);
    private Element searchButtonCategory = new Element(SEARCH_BUTTON);
    private Element cartSectionButtonCategory = new Element(CART_SECTION_BUTTON);
    private Element searchSection = new Element(SEARCH_SECTION_BUTTON);

    public AliHeaderMenuPage clickOnSearchSectionMenu() {
        try {
            searchSection.waitForAppear();
            searchSection.click();

        } catch (WebDriverException e) {
            clickOnSearchSectionMenu();
        }
        return this;
    }

    public AliHeaderMenuPage chooseWatchSection(String category) {
        dropDownSelect.chooseDropDownBox(dropDownSelect, category);
        return this;
    }

    public AliHeaderMenuPage clickSearchButton() {
        searchButtonCategory.click();
        return this;
    }

    public AliHeaderMenuPage clickOnCartSection() {
        cartSectionButtonCategory.waitForVisible();
        cartSectionButtonCategory.click();
        return this;
    }

}
