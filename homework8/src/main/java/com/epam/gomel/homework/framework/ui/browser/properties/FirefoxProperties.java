package com.epam.gomel.homework.framework.ui.browser.properties;

import org.openqa.selenium.firefox.FirefoxProfile;

import java.io.File;

/**
 * Created by Uladzimir Shabetnik on 08.10.2016.
 */
public class FirefoxProperties {
    private static final String DOWNLOAD_PATH = "./src/main/resources/downloads";

    public static FirefoxProfile getFirefoxProperties() {
        FirefoxProfile firefoxProfile = new FirefoxProfile();
        firefoxProfile.setPreference("browser.download.folderList", 2);
        firefoxProfile.setPreference("browser.download.manager.showWhenStarting", false);
        firefoxProfile.setPreference("browser.download.dir", new File(DOWNLOAD_PATH).getAbsolutePath());
        firefoxProfile.setPreference("browser.helperApps.neverAsk.saveToDisk", "text/plain,image/jpeg");
        return firefoxProfile;
    }
}
