package com.epam.gomel.homework.product.aliexpress.purchase.page;

import com.epam.gomel.homework.framework.ui.element.Element;
import org.openqa.selenium.By;

/**
 * Created by Uladzimir Shabetnik on 29.10.2016.
 */
public class AliNavigationSearchResultPage {
    private static final By SEARCH_CATEGORY = By.xpath("//div[@id='aliGlobalCrumb']//h1//span");

    private Element searchResultCategory = new Element(SEARCH_CATEGORY);

    public String getCurrentCategory() {
        return searchResultCategory.getText();
    }
}
