package com.epam.gomel.homework.test.ali;

import com.epam.gomel.homework.framework.ui.browser.Browser;
import com.epam.gomel.homework.product.aliexpress.purchase.page.AliCartPage;
import com.epam.gomel.homework.product.aliexpress.purchase.service.AliPurchaseService;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

/**
 * Created by Uladzimir Shabetnik on 29.10.2016.
 */
public class AliStepsTest {
    private AliPurchaseService aliPurchaseService = new AliPurchaseService();
    private AliCartPage aliCartPage = new AliCartPage();

    @BeforeMethod
    public void setUp() {
        Browser.getBrowser().open("https://ru.aliexpress.com/");
    }

    @Test
    public void aliTest() {
        aliPurchaseService.chooseSearchSection("Часы");
        aliPurchaseService.chooseBrandWatches();
        aliPurchaseService.chooseFirstWatches();
        aliPurchaseService.addPurchaseToCart();
        aliPurchaseService.enterToCartSection();
        aliPurchaseService.deletePurchaseFromCart();
        aliCartPage.checkDeletedPurchaseFromCart();
    }

    @AfterMethod
    public void close() {
        Browser.getBrowser().close();
    }
}
