package com.epam.gomel.homework.framework.ui.browser;

import com.epam.gomel.homework.framework.ui.browser.properties.FirefoxProperties;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.internal.WrapsDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

/**
 * Created by Uladzimir Shabetnik on 08.10.2016.
 */
public final class Browser implements WrapsDriver {
    public static final int ELEMENT_WAIT_TIMEOUT_SECONDS = 20;
    private static Browser instance;
    private WebDriver webDriver;

    private Browser() {
        //webDriver = new ChromeDriver(ChromeProperties.getChromeProperties());
        webDriver = new FirefoxDriver(FirefoxProperties.getFirefoxProperties());
        webDriver.manage().window().maximize();
    }

    public static Browser getBrowser() {
        if (instance == null) {
            instance = new Browser();
        }
        return instance;
    }

    public void close() {
        try {
            if (getWrappedDriver() != null) {
                getWrappedDriver().quit();
            }
        } finally {
            instance = null;
        }
    }

    @Override
    public WebDriver getWrappedDriver() {
        return webDriver;
    }

    public void open(String url) {
        webDriver.get(url);
    }

    public WebElement findElement(By by) {
        return webDriver.findElement(by);
    }

    public Actions getActions() {
        return new Actions(Browser.getBrowser().getWrappedDriver());
    }

    public boolean isElementVisible(By by) {
        return findElement(by).isDisplayed();
    }

    public void waitForElementIsInvisible(By locator) {
        WebDriverWait wait = new WebDriverWait(webDriver, ELEMENT_WAIT_TIMEOUT_SECONDS);
        wait.until(ExpectedConditions.invisibilityOfElementLocated(locator));
    }

    public void waitForElementIsVisible(By locator) {
        WebDriverWait wait = new WebDriverWait(webDriver, ELEMENT_WAIT_TIMEOUT_SECONDS);
        wait.until(ExpectedConditions.visibilityOfElementLocated(locator));
    }

    public void switchToFrameElement(WebElement frame) {
        webDriver.switchTo().frame(frame);
    }

    public void switchToDefaultFame() {
        webDriver.switchTo().defaultContent();
    }

    public void waitForElementIsAppear(By locator) {
        WebDriverWait wait = new WebDriverWait(webDriver, ELEMENT_WAIT_TIMEOUT_SECONDS);
        wait.until(ExpectedConditions.presenceOfElementLocated(locator));
    }

    public void waitForElementIsClickable(By locator) {
        WebDriverWait wait = new WebDriverWait(webDriver, ELEMENT_WAIT_TIMEOUT_SECONDS);
        wait.until(ExpectedConditions.elementToBeClickable(locator));
    }
}
