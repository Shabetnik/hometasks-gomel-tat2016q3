package com.epam.gomel.homework.framework.runner;

import com.epam.gomel.homework.product.aliexpress.purchase.steps.AliPurchaseSteps;
import org.jbehave.core.Embeddable;
import org.jbehave.core.annotations.UsingEmbedder;
import org.jbehave.core.annotations.UsingSteps;
import org.jbehave.core.embedder.Embedder;
import org.jbehave.core.io.CodeLocations;
import org.jbehave.core.io.StoryFinder;
import org.jbehave.core.junit.AnnotatedEmbedderRunner;
import org.junit.Test;
import org.junit.runner.RunWith;
import java.util.List;

/**
 * Created by Uladzimir Shabetnik on 29.10.2016.
 */

@RunWith(AnnotatedEmbedderRunner.class)
@UsingEmbedder(embedder = Embedder.class, generateViewAfterStories = true, metaFilters = "-skip")
@UsingSteps(instances = AliPurchaseSteps.class)
public class StoriesRunner implements Embeddable {
    private Embedder embedder;

    @Override
    public void useEmbedder(Embedder param) {
        this.embedder = param;
    }

    @Test
    @Override
    public void run() throws Throwable {
        embedder.runStoriesAsPaths(storyPaths());
    }

    private List<String> storyPaths() {
        return new StoryFinder().findPaths(CodeLocations.codeLocationFromPath("src/main/resources"),
                "stories/*.story", "");
    }
}

