Feature: do actions with purchase on https://ru.aliexpress.com

Narrative:
As a anonymous user
I want to perform an action on aliexpress with some purchases
So that I can achieve a working functionality of purchase

Scenario: Check any category from navigation ;
Given be on page https://ru.aliexpress.com
When I choose product <category> and click search button
Then I get page <result> with all products from needed category

Examples:
|category         |result|
|Часы             |Часы |
|Обувь            |Обувь|

Scenario: Check extended Search by Brand name ;
Given be on search result of watches page
When I click on Skmei Brand category
Then I get only watches with this brand

Scenario: Check any watches product page ;
Given be on search result of watches page
When I click on any watches
Then I get description page needed watches

Scenario: Add needed product to my Cart section;
Given be on description page needed watches
When I click Add button watches
Then I get needed watches in my Cart

Scenario: Delete needed product from my Cart section;
Given be on Cart page
When I click Delete button watches
Then I get empty Cart section